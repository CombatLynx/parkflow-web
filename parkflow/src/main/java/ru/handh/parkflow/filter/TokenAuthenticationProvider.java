package ru.handh.parkflow.filter;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import ru.handh.parkflow.db.model.auth.TokenAuthEntity;
import ru.handh.parkflow.db.model.exception.TokenAuthException;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.db.repository.TokenAuthRepository;

@Component
@RequiredArgsConstructor
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private final TokenAuthRepository tokenAuthRepository;
    private final MessageSource messageSource;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
        String token = (String) tokenAuthentication.getCredentials();
        if (token == null) {
            throw new TokenAuthException(
                    messageSource.getMessage("auth.token_header_not_found", null, LocaleContextHolder.getLocale()));
        }
        UserEntity user = tokenAuthRepository.findByToken(token).map(TokenAuthEntity::getUser).orElseThrow(() ->
                new TokenAuthException(messageSource.getMessage(
                        "auth.user_with_token_not_found",
                        new Object[]{token},
                        LocaleContextHolder.getLocale()))
        );
        tokenAuthentication.setAuthenticated(true);
        tokenAuthentication.setDetails(user);
        return tokenAuthentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication == TokenAuthentication.class;
    }
}
