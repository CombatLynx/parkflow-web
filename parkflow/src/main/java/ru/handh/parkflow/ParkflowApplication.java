package ru.handh.parkflow;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import ru.handh.parkflow.service.HealthService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication(scanBasePackages = { "ru.handh.parkflow" })
@EntityScan(basePackages = { "ru.handh.parkflow.db.*" })
public class ParkflowApplication {

    private final HealthService healthService;

    public ParkflowApplication(HealthService healthService) {
        this.healthService = healthService;
    }

    @GetMapping("/")
    public String home() {
        return healthService.message();
    }

    public static void main(String[] args) {
        SpringApplication.run(ParkflowApplication.class, args);
    }
}
