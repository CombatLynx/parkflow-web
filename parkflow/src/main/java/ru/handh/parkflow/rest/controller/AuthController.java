package ru.handh.parkflow.rest.controller;

import com.google.i18n.phonenumbers.NumberParseException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.handh.parkflow.db.model.exception.*;
import ru.handh.parkflow.dto.request.ApprovePhoneCodeRequest;
import ru.handh.parkflow.dto.request.PhoneRequest;
import ru.handh.parkflow.dto.response.BaseResponse;
import ru.handh.parkflow.dto.response.GetCountryResponse;
import ru.handh.parkflow.dto.response.GetPhoneCodesResponse;
import ru.handh.parkflow.service.ApprovePhoneCodeService;
import ru.handh.parkflow.service.AuthService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(value = "/auth", description = "API для авторизации")
public class AuthController {

    private final ApprovePhoneCodeService approvePhoneCodeService;
    private final AuthService authService;

    @ApiOperation("Запрос кода для авторизации")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Неверные параметры запроса"
            ),
            @ApiResponse(
                    code = 403,
                    message = "Пользователь деактивирован"
            ),
            @ApiResponse(
                    code = 429,
                    message = "Запросы происходят слишком часто, повторите позже. Частоту см. в параметрах системы"
            ),
    })
    @PostMapping("/code-request")
    public ResponseEntity<BaseResponse> codeRequest(@RequestBody PhoneRequest request) {
        try {
            approvePhoneCodeService.createApprovePhoneCode(request.getPhone());
        } catch(ApproveCodeException e) {
            return new ResponseEntity<>(new BaseResponse(false, 429, e.getMessage()), HttpStatus.TOO_MANY_REQUESTS);
        } catch(UserException e) {
            return new ResponseEntity<>(new BaseResponse(false, 403, e.getMessage()), HttpStatus.FORBIDDEN);
        } catch(NumberParseException | WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Авторизация с помощью номера телефона")
    @ApiResponses({
            @ApiResponse(
                    code = 201,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 403,
                    message = "Пользователь деактивирован"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Некорректные параметры запроса"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Код для авторизации не высылался данному пользователю"
            ),
            @ApiResponse(
                    code = 401,
                    message = "Был введен некорректный код"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Пользователь с данным номером телефона уже существует"
            ),
    })
    @PostMapping
    public ResponseEntity<? extends BaseResponse> authByPhone(@RequestBody @Valid ApprovePhoneCodeRequest request) {
        try {
            return authService.authorize(request.getPhone(), request.getCode());
        } catch(EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 404, e.getMessage()), HttpStatus.NOT_FOUND);
        } catch(ApproveCodeException e) {
            return new ResponseEntity<>(new BaseResponse(false, 401, e.getMessage()), HttpStatus.UNAUTHORIZED);
        } catch(UserException e) {
            return new ResponseEntity<>(new BaseResponse(false, 403, e.getMessage()), HttpStatus.FORBIDDEN);
        } catch(NumberParseException | WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (DuplicatePhoneException e) {
            return new ResponseEntity<>(new BaseResponse(false, 500, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Получение списка телефонных кодов стран")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @GetMapping("/phone-codes")
    public ResponseEntity<GetPhoneCodesResponse> getPhoneCodes() {
        return authService.getPhoneCodes();
    }

    @ApiOperation("Получение списка всех стран")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @GetMapping("/countries")
    public ResponseEntity<GetCountryResponse> getCountries() {
        return authService.getCountry();
    }
}
