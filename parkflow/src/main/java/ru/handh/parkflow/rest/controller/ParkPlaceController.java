package ru.handh.parkflow.rest.controller;

import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.handh.parkflow.db.model.exception.*;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.dto.request.CreateParkPlaceRequest;
import ru.handh.parkflow.dto.request.GetRouteRequest;
import ru.handh.parkflow.dto.request.SearchParkPlaceRequest;
import ru.handh.parkflow.dto.response.BaseResponse;
import ru.handh.parkflow.dto.response.GetLeavingDelaysResponse;
import ru.handh.parkflow.service.ParkPlaceService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/offer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(value = "/offer", description = "API для парковочных мест")
public class ParkPlaceController {

    private final ParkPlaceService parkPlaceService;

    @ApiOperation("Получение всех доступных параметров времени отъезда")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @GetMapping("/leaving-delays")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<GetLeavingDelaysResponse> getLeavingDelays() {
        return parkPlaceService.getLeavingDelays();
    }

    @ApiOperation("Создание ПМ")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Неправильные параметры запроса."
            ),
            @ApiResponse(
                    code = 409,
                    message = "Пользователь слишком далеко от того места, где размещается ПМ"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Сущность не найдена"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Ошибка планировщика задач"
            ),
    })
    @PutMapping
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<? extends BaseResponse> createParkPlace(@Valid @RequestBody CreateParkPlaceRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            return parkPlaceService.createParkPlace(request, user);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 404, e.getMessage()), HttpStatus.NOT_FOUND);
        } catch (SchedulerTasksException e) {
            return new ResponseEntity<>(new BaseResponse(false, 500, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (WrongPositionException e) {
            return new ResponseEntity<>(new BaseResponse(false, 409, e.getMessage()), HttpStatus.CONFLICT);
        } catch (WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation("Поиск ПМ")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Ошибка построения маршрута"
            ),
    })
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<? extends BaseResponse> searchParkPlaces(@RequestBody @Valid SearchParkPlaceRequest request) {
        try {
            return parkPlaceService.searchParkPlaces(request);
        } catch (RouteException e) {
            return new ResponseEntity<>(new BaseResponse(false, 500, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Бронирование указанного ПМ")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Сущность не найдена"
            ),
            @ApiResponse(
                    code = 423,
                    message = "Доступ запрещен"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Ошибка планировщика задач"
            ),
    })
    @PostMapping("/{id}")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<? extends BaseResponse> claimOffer(@PathVariable("id") Long offerId, @RequestParam Long startTime) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            return parkPlaceService.claimOffer(user.getId(), offerId, startTime);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 404, e.getMessage()), HttpStatus.NOT_FOUND);
        } catch (NotAllowedException e) {
            return new ResponseEntity<>(new BaseResponse(false, 423, e.getMessage()), HttpStatus.FORBIDDEN);
        } catch (SchedulerTasksException e) {
            return new ResponseEntity<>(new BaseResponse(false, 500, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Построение маршрута до ПМ")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Сущность не найдена"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Ошибка построения маршрута"
            ),
    })
    @PostMapping("/{id}/route")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<? extends BaseResponse> getRoute(@PathVariable("id") Long offerId, @Valid @RequestBody GetRouteRequest request) {
        try {
            return parkPlaceService.getRoute(offerId, request.getLat(), request.getLon());
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 404, e.getMessage()), HttpStatus.NOT_FOUND);
        } catch (RouteException e) {
            return new ResponseEntity<>(new BaseResponse(false, 500, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Подтвержение указанной сделки")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 423,
                    message = "Доступ запрещен"
            ),
    })
    @PostMapping("/{id}/approve")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> approveOffer(@PathVariable("id") Long id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            parkPlaceService.approveOffer(user.getId(), id);
        } catch (NotAllowedException e) {
            return new ResponseEntity<>(new BaseResponse(false, 423, e.getMessage()), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Отмена ПМ")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Неправильные параметры запроса."
            ),
            @ApiResponse(
                    code = 404,
                    message = "Сущность не найдена"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Ошибка планировщика задач"
            ),
    })
    @PostMapping("/{id}/cancel")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> cancel(@PathVariable("id") Long offerId) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            parkPlaceService.cancel(user.getId(), offerId);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 404, e.getMessage()), HttpStatus.NOT_FOUND);
        } catch (WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (SchedulerTasksException e) {
            return new ResponseEntity<>(new BaseResponse(false, 500, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }
}
