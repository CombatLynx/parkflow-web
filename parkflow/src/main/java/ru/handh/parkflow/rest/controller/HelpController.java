package ru.handh.parkflow.rest.controller;

import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.handh.parkflow.dto.response.BaseResponse;
import ru.handh.parkflow.dto.response.GetHelpAndFaqResponse;
import ru.handh.parkflow.dto.response.GetLegalResponse;
import ru.handh.parkflow.service.HelpService;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/help")
@Api(value = "/help", description = "API для получения справки.")
public class HelpController {

    private final MessageSource messageSource;
    private final HelpService helpService;

    @ApiOperation("Получение справки")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @GetMapping
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<GetHelpAndFaqResponse> getHelpAndFaq(Pageable pageable) {
        return helpService.getHelpAndFaq(pageable);
    }

    @ApiOperation("Получение условий использования")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @GetMapping("/legal")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<GetLegalResponse> getLegal(Pageable pageable) {
        return helpService.getLegal(pageable);
    }

    @ApiOperation("Пользовательское соглашение")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Внутренняя ошибка сервера"
            ),
    })
    @GetMapping(
            value = "/terms-of-service",
            produces = "application/pdf"
    )
    public ResponseEntity getTermsOfService() {
        try {
            return helpService.getTermsOfService();
        } catch(IOException e) {
            return new ResponseEntity<>(
                    new BaseResponse(
                            false,
                            500,
                            messageSource.getMessage("terms_of_service.error", null, LocaleContextHolder.getLocale())),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }
}
