package ru.handh.parkflow.rest.controller;

import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.handh.parkflow.dto.request.FeedbackRequest;
import ru.handh.parkflow.dto.response.BaseResponse;
import ru.handh.parkflow.dto.response.GetFeedbackReasonResponse;
import ru.handh.parkflow.dto.response.GetFeedbackResponse;
import ru.handh.parkflow.service.FeedbackService;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/feedback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(value = "/feedback", description = "API для отзывов о приложении")
public class FeedbackController {

    private final FeedbackService feedbackService;

    @ApiOperation("Оставить отзыв")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @PostMapping
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> feedbackRequest(@RequestBody FeedbackRequest feedbackRequest) {
        feedbackService.sendFeedback(feedbackRequest.getReason(), feedbackRequest.getMessage());

        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Получение всех отзывов о приложении")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @GetMapping
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<GetFeedbackResponse> getFeedback() {
        return feedbackService.getFeedback();
    }

    @ApiOperation("Получение всех доступных причин")
    @GetMapping(value = "/feedback-reason", produces = "application/json;charset=UTF-8")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<GetFeedbackReasonResponse> getFeedbackReason() {
        return feedbackService.getFeedbackReason();
    }

}
