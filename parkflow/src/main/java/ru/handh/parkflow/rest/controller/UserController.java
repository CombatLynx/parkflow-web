package ru.handh.parkflow.rest.controller;

import com.google.i18n.phonenumbers.NumberParseException;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.handh.parkflow.db.model.exception.*;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.dto.CarDto;
import ru.handh.parkflow.dto.CoordinateDto;
import ru.handh.parkflow.dto.request.*;
import ru.handh.parkflow.dto.response.BaseResponse;
import ru.handh.parkflow.dto.response.GetCarColorsResponse;
import ru.handh.parkflow.service.PositionService;
import ru.handh.parkflow.service.UserService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user")
@Api(value = "/user", description = "API для управления пользователями.")
public class UserController {

    private final UserService userService;
    private final PositionService positionService;

    @ApiOperation("Получение всех доступных цветов автомобиля")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @GetMapping("/get-car-colors")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<GetCarColorsResponse> getCarColors() {
        return userService.getCarColors();
    }

    @ApiOperation("Получение информации об указанном пользователе (себе или в рамках доступных сделок)")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Пользователь по переданному ID не найден"
            ),
    })
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<? extends  BaseResponse> getUserInfo(@PathVariable("id") Long id,
                                                           @RequestParam(value = "offer_id", required = false) Long offerId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            return userService.getOtherUserById(user.getId(), id, offerId);
        } catch(EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 404, e.getMessage()), HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation("Обновление информации о пользователе")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Неверные параметры запроса"
            ),
    })
    @PatchMapping
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updateUserInfo(@Valid @RequestBody UpdateUserRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            userService.updateUser(user.getId(), request.getFirstName(), request.getLastName(), request.getEmail(), request.getCar());
        } catch (WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Обновление имени пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-first-name")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updateFirstName(@Valid @RequestBody UpdateFirstNameRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        userService.updateFirstName(user.getId(), request.getFirstName());
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Обновление фамилии пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-last-name")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updateLastName(@Valid @RequestBody UpdateLastNameRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        userService.updateLastName(user.getId(), request.getLastName());
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Запрос кода для обновления номера телефона пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Неверные параметры запроса"
            ),
            @ApiResponse(
                    code = 403,
                    message = "Пользователь деактивирован"
            ),
            @ApiResponse(
                    code = 429,
                    message = "Запросы происходят слишком часто, повторите позже. Частоту см. в параметрах системы"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Пользователь с данным номером телефона уже существует"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-phone-request")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updatePhoneRequest(@Valid @RequestBody PhoneRequest phone) {
        try {
            userService.updatePhoneRequest(phone.getPhone());
        } catch (NumberParseException | WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApproveCodeException e) {
            return new ResponseEntity<>(new BaseResponse(false, 429, e.getMessage()), HttpStatus.TOO_MANY_REQUESTS);
        } catch (UserException e) {
            return new ResponseEntity<>(new BaseResponse(false, 403, e.getMessage()), HttpStatus.FORBIDDEN);
        } catch (DuplicatePhoneException e) {
            return new ResponseEntity<>(new BaseResponse(false, 500, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Подтверждение кода для обновления номера телефона пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Неверные параметры запроса"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Код для авторизации не высылался данному пользователю"
            ),
            @ApiResponse(
                    code = 429,
                    message = "Запросы происходят слишком часто, повторите позже. Частоту см. в параметрах системы"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-phone-confirm")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updatePhoneConfirm(@Valid @RequestBody ApprovePhoneCodeRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            userService.updatePhoneConfirm(user.getId(), request.getPhone(), request.getCode());
        } catch (NumberParseException | WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApproveCodeException e) {
            return new ResponseEntity<>(new BaseResponse(false, 429, e.getMessage()), HttpStatus.TOO_MANY_REQUESTS);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 404, e.getMessage()), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Обновление почты пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Неверные параметры запроса"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-email")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updateEmail(@Valid @RequestBody EmailRequest emailRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            userService.updateEmail(user.getId(), emailRequest.getEmail());
        } catch (WrongArgumentException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Обновление страны пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Некорректные параметры запроса"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-country")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updateCountry(@RequestBody UpdateCountryRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        try {
            userService.updateCountry(user.getId(), request.getCountry().getId());
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new BaseResponse(false, 400, e.getMessage()), HttpStatus.BAD_REQUEST);
            //e.printStackTrace();
        }
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Обновление информации об автомобиле пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-car")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updateCar(@Valid @RequestBody CarDto carRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        userService.updateCar(user.getId(), carRequest);
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Обновление поля havePermit")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @RequestMapping(method = RequestMethod.PATCH, value = "/update-have-permit")
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    public ResponseEntity<BaseResponse> updateHavePermit(@Valid @RequestBody UpdateHavePermitRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        userService.updateHavePermit(user.getId(), request.getHavePermit());
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }


    @ApiOperation("Обновление позиции указанного пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @ApiImplicitParam(name = "Token-Auth", required = true, value = "Token string", paramType = "header")
    @PostMapping("/coordinates")
    public ResponseEntity<CoordinateDto> updateUserPosition(@Valid @RequestBody CoordinateDto coordinateDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        return positionService.updatePosition(user.getId(), coordinateDto);
    }

    @ApiOperation("Сохранение Push-токена пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @PostMapping(value = "/push")
    public ResponseEntity<BaseResponse> savePush(@Valid @RequestBody PushRequest pushRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        userService.savePush(user.getId(), pushRequest.getPlatform(), pushRequest.getPushToken());
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @ApiOperation("Удаление Push-токена указанного пользователя")
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Успешно"
            ),
    })
    @DeleteMapping(value = "/push")
    public ResponseEntity<BaseResponse> deletePush() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = (UserEntity) auth.getDetails();
        userService.deletePush(user.getId());
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }
}
