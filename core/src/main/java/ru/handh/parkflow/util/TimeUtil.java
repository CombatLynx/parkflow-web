package ru.handh.parkflow.util;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class TimeUtil {

    /**
     * перевод из epoch в OffsetDateTime средствами класса OffsetDateTime
     *
     * @param timestamp epoch - время в секундах
     * @return время в OffsetDateTime
     */
    public static OffsetDateTime ofEpoch(Long timestamp) {
        if (timestamp == null) {
            return null;
        }
        return OffsetDateTime.of(
                LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC),
                ZoneOffset.UTC);
    }

    public static Boolean isTimeBeforeNow(Long time) {
        return time < OffsetDateTime.now().toEpochSecond();
    }
}
