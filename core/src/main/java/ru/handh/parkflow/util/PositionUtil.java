package ru.handh.parkflow.util;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import ru.handh.parkflow.db.model.exception.WrongArgumentException;
import ru.handh.parkflow.db.model.property.PropertyName;
import ru.handh.parkflow.dto.CoordinateDto;
import ru.handh.parkflow.service.PropertyService;

@Component
@RequiredArgsConstructor
public class PositionUtil {

    private final PropertyService propertyService;
    private final MessageSource messageSource;

    /**
     * проверяет, находится ли точка на допустимом расстоянии от местоположения. Если accuracy объекта
     * coordinateDto - null, то берется значение из таблицы параметров
     *
     * @param coordinateDto - местоположение
     * @param lat           - широта проверяемой точки
     * @param lon           - долгота проверяемой точки
     * @return true, если точка находится на допустимом расстоянии
     */
    public boolean rightPosition(CoordinateDto coordinateDto, Double lat, Double lon) throws WrongArgumentException {
        if((coordinateDto.getLat()==null) || (coordinateDto.getLon()==null)){
            return false;
        }
        Integer defAccuracy = coordinateDto.getAccuracy();
        lat = rad(lat);
        lon = rad(lon);
        Double lat1 = rad(coordinateDto.getLat()), lon1 = rad(coordinateDto.getLon());
        if (defAccuracy == null) {
            defAccuracy = Integer.valueOf(propertyService.getByName(PropertyName.DEF_ACCURACY.name()).getValue());
        }
        Double accuracy = countDistance(lat, lat1, lon, lon1);
        return accuracy <= defAccuracy;
    }

    /**
     * получает расстояние между точками
     *
     * @param lat  - широта первой точки
     * @param lat1 - широта второй точки
     * @param lon  - долгота первой точки
     * @param lon1 - широта второй точки
     * @return расстояние
     * @throws WrongArgumentException - возникает при долготе не из диапазона (-180; 180)
     *                                        или широте не из диапазона (-90; 90)
     */
    public Double countDistance(Double lat, Double lat1, Double lon, Double lon1)
            throws WrongArgumentException {
        if((lat==null)||(lon==null)||(lat1==null)||(lon1==null)){
            return Double.valueOf(propertyService.getByName(PropertyName.CANCEL_OFFER_DISTANCE.name()).getValue()) + 100;
        }
        if ((lon > 180) || (lon < -180) || (lat > 90) || (lat < -90) ||
                (lon1 > 180) || (lon1 < -180) || (lat1 > 90) || (lat1 < -90)) {
            throw new WrongArgumentException(messageSource.getMessage(
                    "park_place.wrong_position",
                    null,
                    LocaleContextHolder.getLocale()
            ));
        }
        lat = rad(lat);
        lon = rad(lon);
        lat1 = rad(lat1);
        lon1 = rad(lon1);
        return 2 * 6367444.6571225 *
                Math.asin(
                        Math.sqrt(
                                Math.pow(Math.sin((lat - lat1) / 2), 2) +
                                        Math.cos(lat) * Math.cos(lat1) * Math.pow(Math.sin((lon - lon1) / 2), 2)
                        )
                );
    }

    /**
     * переводит градусы в радианы
     * @param d - координата в градусах
     * @return угол в радианах
     */
    private Double rad(Double d) {
        return d * Math.PI / 180;
    }
}
