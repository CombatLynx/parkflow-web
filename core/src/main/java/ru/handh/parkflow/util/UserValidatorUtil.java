package ru.handh.parkflow.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import ru.handh.parkflow.db.model.exception.WrongArgumentException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidatorUtil {

    private static final Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    public static void validatePhone(String phone, MessageSource messageSource) throws WrongArgumentException, NumberParseException {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber number = phoneUtil.parse(phone, null);
            boolean isValid = phoneUtil.isValidNumber(number);
            if(!isValid) {
                throw new WrongArgumentException(messageSource.getMessage("invalid_phone", null, LocaleContextHolder.getLocale()));
            }
        } catch (NumberParseException e) {
            throw new NumberParseException(
                    e.getErrorType(),
                    messageSource.getMessage("phone.can_not_parse", null, LocaleContextHolder.getLocale())
            );
        }
    }

    public static void validateEmail(String email, MessageSource messageSource) throws WrongArgumentException {
        Matcher matcher = emailPattern.matcher(email);
        if(!matcher.matches()) {
            throw new WrongArgumentException(messageSource.getMessage("invalid_email", null, LocaleContextHolder.getLocale()));
        }
    }
}
