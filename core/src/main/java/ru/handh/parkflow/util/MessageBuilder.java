package ru.handh.parkflow.util;

import org.springframework.stereotype.Service;
import ru.handh.parkflow.db.model.message.SmsMessageEntity;

public class MessageBuilder {

    public SmsMessageEntity buildSms(String to, String text) {
        return new SmsMessageEntity(to, text);
    }
}
