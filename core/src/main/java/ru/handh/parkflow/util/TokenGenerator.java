package ru.handh.parkflow.util;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TokenGenerator {

    public String generateToken() {
        int tokenSize = 36;
        int n = (tokenSize /36) + 1;

        StringBuilder uuid = new StringBuilder();
        for(int i=0; i<n; i++){
            uuid.append(UUID.randomUUID().toString());
        }
        return uuid.substring(0, tokenSize);
    }
}
