package ru.handh.parkflow.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.Objects;

@RequiredArgsConstructor
public abstract class AbstractDtoMapper<E, D> implements EntityDtoMapper<E, D> {

    protected Class<E> entityClass;
    protected Class<D> dtoClass;

    protected final ModelMapper modelMapper;

    @Override
    public E convertToEntity(final D dto) {
        return Objects.isNull(dto) ? null : modelMapper.map(dto, entityClass);
    }

    @Override
    public E convertToEntity(final D dto, final E entity) {
        if (Objects.isNull(dto)) {
            return entity;
        }
        if (Objects.isNull(entity)) {
            return convertToEntity(dto);
        }
        modelMapper.map(dto, entity);
        return entity;
    }

    @Override
    public D convertToDto(final E entity) {
        return Objects.isNull(entity) ? null : modelMapper.map(entity, dtoClass);
    }

    protected Converter<E, D> toDtoConverter() {
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            toDtoConverter(source, destination);
            return destination;
        };
    }

    protected Converter<D, E> toEntityConverter() {
        return context -> {
            D source = context.getSource();
            E destination = context.getDestination();
            toEntityConverter(source, destination);
            return destination;
        };
    }

    protected void toDtoConverter(final E entity, final D dto) {
    }

    protected void toEntityConverter(final D dto, final E entity) {
    }

    /**
     * A method must have a {@link javax.annotation.PostConstruct} annotation
     * or it should be called in a constructor of a subclass.
     */
    public abstract void setupMapper();

    @PostConstruct
    @SuppressWarnings("unchecked")
    public void init() {
        entityClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        dtoClass = (Class<D>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }
}
