package ru.handh.parkflow.mapper;

import org.springframework.data.domain.Page;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public interface EntityDtoMapper<E, D> {

    E convertToEntity(final D dto);

    E convertToEntity(final D dto, final E entity);

    D convertToDto(final E entity);

    default List<E> convertToEntity(final List<D> d) {
        return convertToEntity(d, false);
    }

    default List<E> convertToEntity(final List<D> e, final boolean isParallel) {
        if (CollectionUtils.isEmpty(e)) return Collections.emptyList();
        return StreamSupport.stream(e.spliterator(), isParallel)
                .map(this::convertToEntity)
                .collect(Collectors.toList());
    }

    default Page<D> convertToDto(final Page<E> e) {
        return e.map(this::convertToDto);
    }

    default List<D> convertToDto(final List<E> e) {
        return convertToDto(e, false);
    }

    default List<D> convertToDto(final List<E> e, final boolean isParallel) {
        if (CollectionUtils.isEmpty(e)) return Collections.emptyList();
        return StreamSupport.stream(e.spliterator(), isParallel)
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    default Set<D> convertToDto(final Set<E> e) {
        return convertToDto(e, false);
    }

    default Set<D> convertToDto(final Set<E> e, final boolean isParallel) {
        if (CollectionUtils.isEmpty(e)) return Collections.emptySet();
        return StreamSupport.stream(e.spliterator(), isParallel)
                .map(this::convertToDto)
                .collect(Collectors.toSet());
    }

    default <T> void whenNotNull(final T o, final Consumer<T> c) {
        whenNotNull(o, c, null);
    }

    default <T> void whenNotNull(final T o, final Consumer<T> c, final Action nullAction) {
        when(o, Objects::nonNull, c, nullAction);
    }

    default <T> void whenNot(final T o, final Predicate<T> p, final Consumer<T> c) {
        when(o, p.negate(), c, null);
    }

    default <T> void when(final T o, final Predicate<T> p, final Action action) {
        if (p.test(o)) action.execute();
    }

    default <T> void when(final T o, final Predicate<T> p, final Consumer<T> c) {
        when(o, p, c, null);
    }

    default <T> void when(final T o, final Predicate<T> p,
                          final Consumer<T> c, final Action nullAction) {
        if (p.test(o)) {
            c.accept(o);
        } else {
            if (nullAction != null) {
                nullAction.execute();
            }
        }
    }

    @FunctionalInterface
    interface Action {
        void execute();
    }
}
