package ru.handh.parkflow.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.handh.parkflow.db.model.parking.ParkPlaceEntity;
import ru.handh.parkflow.dto.ParkPlaceDto;
import ru.handh.parkflow.dto.UserInfoDto;

import javax.annotation.PostConstruct;
import java.util.Date;

@Component
public class ParkPlaceDtoMapper extends AbstractDtoMapper<ParkPlaceEntity, ParkPlaceDto> {

    private final MessageSource messageSource;

    public ParkPlaceDtoMapper(ModelMapper modelMapper, MessageSource messageSource) {
        super(modelMapper);
        this.messageSource = messageSource;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(entityClass, dtoClass)
                .addMappings(mapping -> {
                    mapping.skip(ParkPlaceDto::setCreateTime);
                    mapping.skip(ParkPlaceDto::setOfferUserId);
                    mapping.skip(ParkPlaceDto::setClaimUserId);
                    mapping.skip(ParkPlaceDto::setOfferUserInfo);
                    mapping.skip(ParkPlaceDto::setClaimUserInfo);
                    mapping.skip(ParkPlaceDto::setType);
                })
                .setPostConverter(toDtoConverter());
    }

    @Override
    protected void toDtoConverter(final ParkPlaceEntity entity, final ParkPlaceDto dto) {
        whenNotNull(entity.getCreateTime(), time -> dto.setCreateTime(Date.from(time.toInstant()).getTime()));
        whenNotNull(entity.getOfferUser(), user -> {
            dto.setOfferUserId(user.getId());
            dto.setOfferUserInfo(new UserInfoDto(user, messageSource));
        });
        whenNotNull(entity.getClaimUser(), user -> {
            dto.setClaimUserId(user.getId());
            dto.setClaimUserInfo(new UserInfoDto(user, messageSource));
        });
        whenNotNull(entity.getType(), type -> dto.setType(type.name()));
    }
}
