package ru.handh.parkflow.service;

import com.google.i18n.phonenumbers.NumberParseException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.db.model.auth.ApprovePhoneCodeEntity;
import ru.handh.parkflow.db.model.auth.TokenAuthEntity;
import ru.handh.parkflow.db.model.exception.ApproveCodeException;
import ru.handh.parkflow.db.model.exception.EntityNotFoundException;
import ru.handh.parkflow.db.model.exception.UserException;
import ru.handh.parkflow.db.model.exception.WrongArgumentException;
import ru.handh.parkflow.db.model.message.SmsMessageEntity;
import ru.handh.parkflow.db.model.property.PropertyName;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.db.repository.ApprovePhoneCodeRepository;
import ru.handh.parkflow.db.repository.UserRepository;
import ru.handh.parkflow.service.message.SmsMessageService;
import ru.handh.parkflow.util.CodeGenerator;
import ru.handh.parkflow.util.UserValidatorUtil;

import java.time.OffsetDateTime;
import java.util.Optional;

@Transactional
@Service
@RequiredArgsConstructor
public class ApprovePhoneCodeService {

    private final MessageSource messageSource;
    private final ApprovePhoneCodeRepository approvePhoneCodeRepository;
    private final UserRepository userRepository;
    private final PropertyService propertyService;
    private final SmsMessageService smsMessageService;

    public void createApprovePhoneCode(String phone) throws ApproveCodeException, UserException, NumberParseException, WrongArgumentException {
        Optional<UserEntity> userOptional = userRepository.findByPhone(phone);
        if(userOptional.isPresent() && !userOptional.get().isActive()) {
            throw new UserException(messageSource.getMessage(
                    "user.deactivated",
                    null,
                    LocaleContextHolder.getLocale()
            ));
        }

        UserValidatorUtil.validatePhone(phone, messageSource);
        ApprovePhoneCodeEntity code = approvePhoneCodeRepository.findByPhone(phone).orElseGet(() -> {
            ApprovePhoneCodeEntity e = new ApprovePhoneCodeEntity();
            e.setPhone(phone);
            return  e;
        });
        Long diff = checkCodeTimeout(code);
        if (diff <= 0) {
            refreshApprovePhoneCodeData(code);
            sendCode(code);
            return;
        }
        throw new ApproveCodeException(
                messageSource.getMessage("auth.code_timeout", null, LocaleContextHolder.getLocale())
        );
    }

    private Long checkCodeTimeout(ApprovePhoneCodeEntity code) {
        if (code.getLastRequest() == null) {
            return 0L;
        }
        return (code.getLastRequest().toEpochSecond()
                        + Long.valueOf(propertyService.getByName(PropertyName.CODE_REQUEST_RANGE.name()).getValue())
                        - OffsetDateTime.now().toEpochSecond());
    }

    private void refreshApprovePhoneCodeData(ApprovePhoneCodeEntity code) {
        code.setLastRequest(OffsetDateTime.now());
        code.setCode(CodeGenerator.generateCode());
        approvePhoneCodeRepository.save(code);
    }

    private void sendCode(ApprovePhoneCodeEntity code) {
        smsMessageService.scheduleMessage(new SmsMessageEntity(code.getPhone(), code.getCode()));
    }

    public void checkApprovePhoneCode(String phone, String code)
            throws EntityNotFoundException, ApproveCodeException, NumberParseException, WrongArgumentException {

        UserValidatorUtil.validatePhone(phone, messageSource);
        ApprovePhoneCodeEntity approveCode = approvePhoneCodeRepository.findByPhone(phone).orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage(
                        "auth.code_not_found",
                        null,
                        LocaleContextHolder.getLocale()))
        );
        approvePhoneCodeRepository.delete(approveCode);
        if (!approveCode.getCode().equals(code)) {
            throw new ApproveCodeException(
                    messageSource.getMessage("auth.wrong_code", null, LocaleContextHolder.getLocale()));
        }
    }

}
