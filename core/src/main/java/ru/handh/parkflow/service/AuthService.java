package ru.handh.parkflow.service;

import com.google.i18n.phonenumbers.NumberParseException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.db.model.auth.PhoneCodeEntity;
import ru.handh.parkflow.db.model.auth.TokenAuthEntity;
import ru.handh.parkflow.db.model.exception.*;
import ru.handh.parkflow.db.model.user.CountryEntity;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.db.repository.CountryRepository;
import ru.handh.parkflow.db.repository.TokenAuthRepository;
import ru.handh.parkflow.db.repository.PhoneCodeRepository;
import ru.handh.parkflow.db.repository.UserRepository;
import ru.handh.parkflow.dto.CountryDto;
import ru.handh.parkflow.dto.PhoneCodeDto;
import ru.handh.parkflow.dto.TokenAuthDto;
import ru.handh.parkflow.dto.response.GetCountryResponse;
import ru.handh.parkflow.dto.response.GetPhoneCodesResponse;
import ru.handh.parkflow.dto.response.TokenAuthResponse;
import ru.handh.parkflow.util.TokenGenerator;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final MessageSource messageSource;
    private final TokenAuthRepository tokenAuthRepository;
    private final PhoneCodeRepository phoneCodeRepository;
    private final UserRepository userRepository;
    private final CountryRepository countryRepository;
    private final TokenGenerator tokenGenerator;
    private final ApprovePhoneCodeService approvePhoneCodeService;
    private final PropertyService propertyService;
    private final UserService userService;

    @Transactional
    public ResponseEntity<GetPhoneCodesResponse> getPhoneCodes() {

        List<PhoneCodeEntity> phoneCode = phoneCodeRepository.findAll();

        return new ResponseEntity<>(
                new GetPhoneCodesResponse(phoneCode
                    .stream()
                    .map(entity -> new PhoneCodeDto(entity, messageSource))
                    .collect(Collectors.toList())),
                HttpStatus.OK
        );
    }

    @Transactional
    public ResponseEntity<GetCountryResponse> getCountry() {

        List<CountryEntity> country = countryRepository.findAll();

        return new ResponseEntity<>(
                new GetCountryResponse(country
                        .stream()
                        .map(entity -> new CountryDto(entity, messageSource))
                        .collect(Collectors.toList())),
                HttpStatus.OK
        );
    }

    @Transactional
    public ResponseEntity<TokenAuthResponse> authorize(String phone, String code)
            throws EntityNotFoundException, ApproveCodeException, UserException, NumberParseException, WrongArgumentException, DuplicatePhoneException {

        approvePhoneCodeService.checkApprovePhoneCode(phone, code);

        Optional<UserEntity> userOptional = userRepository.findByPhone(phone);
        if(userOptional.isPresent() && !userOptional.get().isActive()) {
            throw new UserException(messageSource.getMessage(
                    "user.deactivated",
                    null,
                    LocaleContextHolder.getLocale()
            ));
        }
        UserEntity userEntity;
        if(userOptional.isPresent()) {
            userEntity = userOptional.get();
        } else {
            userEntity = userService.registerUserByPhone(phone);
        }

        TokenAuthDto tokenAuthDto = new TokenAuthDto();
        tokenAuthDto.setId(userEntity.getId());
        TokenAuthEntity tokenAuth = createToken(userEntity);
        tokenAuthDto.setToken(tokenAuth.getToken());
        tokenAuthDto.setTokenExp(tokenAuth.getExpired().toEpochSecond());
        tokenAuthDto.setCacheVersion(propertyService.getCacheVersion());
        try {
            tokenAuthDto.setNeedUserInfo(userService.getAccountState(userEntity.getId()).getFillUserInfoRequired());
        } catch (EntityNotFoundException e) {
            //e.printStackTrace();
        }
        return new ResponseEntity<>(new TokenAuthResponse(tokenAuthDto), HttpStatus.OK);
    }

    @Transactional
    public TokenAuthEntity createToken(UserEntity e) {
        TokenAuthEntity authToken = tokenAuthRepository.findByUser(e).orElseGet(() -> {
            TokenAuthEntity token = new TokenAuthEntity();
            token.setUser(e);
            return token;
        });
        OffsetDateTime expired = OffsetDateTime.of(LocalDate.now().plusMonths(1), LocalTime.now(), ZoneOffset.MIN);
        authToken.setExpired(expired);
        authToken.setToken(tokenGenerator.generateToken());
        tokenAuthRepository.save(authToken);
        return authToken;
    }
}
