package ru.handh.parkflow.service.message;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.stereotype.Service;
import ru.handh.parkflow.db.model.message.SmsMessageEntity;
import ru.handh.parkflow.db.model.message.SmsQueueEntity;
import ru.handh.parkflow.db.repository.message.SmsMessageRepository;
import ru.handh.parkflow.db.repository.message.SmsQueueRepository;

import java.net.URI;

@Service
public class SmsMessageService extends AbstractMessageService<SmsMessageEntity, SmsQueueEntity> {

    private static final String ACCOUNT_SID = "AC0558386c044fef07d0f62477e2e32560";
    private static final String AUTH_TOKEN = "ff8065dcf368d02afc6598c2685bf512";
    private static final String FROM = "+447479272750";

    protected SmsMessageService(SmsQueueRepository messageQueueRepository, SmsMessageRepository messageRepository) {
        super(messageQueueRepository, messageRepository);
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

    @Override
    protected SmsQueueEntity createQueueEntry() {
        return new SmsQueueEntity();
    }

    @Override
    protected void sendMessage(SmsMessageEntity messageEntity) {
        Message.creator(
                new PhoneNumber(messageEntity.getTo()),
                new PhoneNumber(FROM),
                messageEntity.getText())
                .create();
    }
}
