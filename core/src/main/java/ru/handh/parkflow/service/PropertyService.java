package ru.handh.parkflow.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.db.model.property.PropertyName;
import ru.handh.parkflow.db.repository.PropertyRepository;
import ru.handh.parkflow.dto.PropertyDto;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Transactional
@Service
@RequiredArgsConstructor
public class PropertyService {

    private final PropertyRepository propertyRepository;

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();

    public PropertyDto getByName(String name) {
        readLock.lock();
        try {
            return new PropertyDto(propertyRepository.findByName(PropertyName.valueOf(name)));
        } finally {
            readLock.unlock();
        }
    }

    public Long getCacheVersion() {
        readLock.lock();
        try {
            return Long.valueOf(propertyRepository.findByName(PropertyName.CACHE_VERSION).getValue());
        } finally {
            readLock.unlock();
        }
    }
}
