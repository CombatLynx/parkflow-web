package ru.handh.parkflow.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.handh.parkflow.db.model.exception.RouteException;
import ru.handh.parkflow.db.model.property.PropertyName;
import ru.handh.parkflow.dto.ParkPlaceDto;
import ru.handh.parkflow.dto.RouteDto;
import ru.handh.parkflow.dto.SearchParkPlaceDto;
import ru.handh.parkflow.dto.MatrixApiResponseDto;
import ru.handh.parkflow.dto.RouteResponseDto;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class RouteService {

    private static final String TOKEN = "pk.eyJ1IjoicGFya2xvb2siLCJhIjoiY2psY3FsM3drMDNicTNwcGhxbG5hOG9oMyJ9.PK3Ig1vBPQvkv8-ev2mjoA";
    private static final int MAX_COORDINATES_PER_REQUEST = 10;
    private static RestTemplate restTemplate = new RestTemplate();

    private final MessageSource messageSource;
    private final PropertyService propertyService;

    public RouteDto getRoute(Double startLat, Double startLon, Double endLat, Double endLon) throws RouteException {
        //TODO: driving or driving-traffic?
        String url = "https://api.mapbox.com/directions/v5/mapbox/driving-traffic/"
                + startLon.toString() + "," + startLat.toString() + ";"
                + endLon.toString() + "," + endLat.toString();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("overview", "full")
                .queryParam("annotations", "duration,distance")
                .queryParam("access_token", TOKEN);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<RouteResponseDto> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                RouteResponseDto.class);

        RouteResponseDto routeResponse = response.getBody();

        if(routeResponse == null || !routeResponse.getCode().equals("Ok")) {
            throw new RouteException(messageSource.getMessage(
                    "route.error",
                    null,
                    LocaleContextHolder.getLocale()
            ));
        }
        RouteDto route = routeResponse.getRoutes().get(0);
        route.setDistance(route.getDistance());

        return route;
    }

    public List<SearchParkPlaceDto> getNearestParkPlaces(Double startLat, Double startLon, List<ParkPlaceDto> parkPlaces) throws RouteException {
        //TODO: driving or driving-traffic?
        String url = "https://api.mapbox.com/directions-matrix/v1/mapbox/driving-traffic/" + startLon +  "," + startLat;
        int requestCount = parkPlaces.size() / (MAX_COORDINATES_PER_REQUEST-1);
        if(parkPlaces.size() % (MAX_COORDINATES_PER_REQUEST-1) != 0) {
            requestCount++;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        /*
         * Разбиваем на несколько запросов из-за ограничения на количество координат в одном запросе
         * Результаты всех запросов сведем в resultDurations
         */
        Map<Integer, Double> resultDurations = new HashMap<>();
        List<Double> resultDistances = new ArrayList<>();

        for(int i = 0; i < requestCount*MAX_COORDINATES_PER_REQUEST; i += MAX_COORDINATES_PER_REQUEST) {
            StringJoiner request = new StringJoiner(";");
            request.add(url);

            List<ParkPlaceDto> subList = parkPlaces.subList(i, Math.min(i + MAX_COORDINATES_PER_REQUEST, parkPlaces.size()));
            subList.forEach(parkPlace ->
                    request
                        .add(parkPlace.getLon().toString() + "," + parkPlace.getLat().toString())
            );

            StringJoiner destinations = new StringJoiner(";");
            subList.forEach(parkPlace -> destinations.add(String.valueOf(subList.indexOf(parkPlace)+1)));

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(request.toString())
                    .queryParam("sources", 0)
                    .queryParam("destinations", destinations)
                    .queryParam("annotations", "distance,duration")
                    .queryParam("access_token", TOKEN);

            ResponseEntity<MatrixApiResponseDto> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    entity,
                    MatrixApiResponseDto.class);

            MatrixApiResponseDto matrixResponse = response.getBody();

            if(matrixResponse == null || !matrixResponse.getCode().equals("Ok")) {
                throw new RouteException(messageSource.getMessage(
                        "route.can_not_find_fastest",
                        null,
                        LocaleContextHolder.getLocale()
                ));
            }
            resultDistances.addAll(matrixResponse.getDistances().get(0));

            List<Double> responseDurations = matrixResponse.getDurations().get(0);
            int offset = i;
            IntStream.range(0, responseDurations.size())
                    .boxed()
                    .forEach(index -> resultDurations.put(index + offset, responseDurations.get(index)));
        }
        int parkPlacesCount = Integer.valueOf(propertyService.getByName(PropertyName.SEARCH_PARK_PLACES_COUNT.name()).getValue());

        return resultDurations
                .entrySet()
                .stream()
                .sorted(Comparator.comparingDouble(Map.Entry::getValue))
                .limit(parkPlacesCount)
                .map(duration -> new SearchParkPlaceDto(
                        parkPlaces.get(duration.getKey()),
                        duration.getValue(),
                        resultDistances.get(duration.getKey())))
                .collect(Collectors.toList());
    }
}
