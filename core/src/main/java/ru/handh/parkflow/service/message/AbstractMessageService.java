package ru.handh.parkflow.service.message;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.db.model.message.BaseMessageEntity;
import ru.handh.parkflow.db.model.message.MessageQueueEntity;
import ru.handh.parkflow.db.model.message.MessageStatus;

import java.time.OffsetDateTime;
import java.util.LinkedList;
import java.util.Queue;

//TODO: log
public abstract class AbstractMessageService<T extends BaseMessageEntity, R extends MessageQueueEntity<T>> {

    private final Integer MESSAGE_MAX_ATTEMPTS = 10;

    @Getter
    protected Queue<R> messageQueue;
    protected final JpaRepository<R, Long> messageQueueRepository;
    protected final JpaRepository<T, Long> messageRepository;

    private final Class<?>[] genericType;

    protected AbstractMessageService(JpaRepository<R, Long> messageQueueRepository, JpaRepository<T, Long> messageRepository) {
        this.messageQueueRepository = messageQueueRepository;
        this.messageRepository = messageRepository;
        this.messageQueue = new LinkedList<>();

        this.genericType = GenericTypeResolver.resolveTypeArguments(getClass(), AbstractMessageService.class);
    }

    /**
     * Помещает сообщение в очередь на отправку.<br>
     * Сообщение и объект очереди помещаютяс в базу.
     *
     * @param message сообщение.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void scheduleMessage(T message) {
        if (message.getTo() == null || message.getTo().isEmpty()) {
            return;
        }
        message.setStatus(MessageStatus.NEW);

        R queueEntry = createQueueEntry();
        queueEntry.setAttempts(0);
        queueEntry.setMessage(message);

        messageRepository.save(message);
        messageQueueRepository.save(queueEntry);
    }

    /**
     * Рассылка очереди.<br>
     * Выбираются все элементы очереди из бд и рассылаются.<br>
     * При превышении количества попыток отпарвки сообщение помечается статусом {@link MessageStatus#ERROR ERROR}
     * и больше не отправляется.
     */
    @Transactional
    public void sendQueue() {
        synchronized (messageQueue) {

            messageQueue = new LinkedList<>(messageQueueRepository.findAll());

            while (messageQueue.size() > 0) {
                R queueEntry = messageQueue.remove();
                T message = queueEntry.getMessage();

                try {
                    this.sendMessage(message);
                    message.setSendTime(OffsetDateTime.now());
                    message.setStatus(MessageStatus.SENDED);
                    messageRepository.save(message);
                    messageQueueRepository.delete(queueEntry);
                } catch (Exception ex) {
                    message.setError(String.format("[%s] %s", ex.getClass().getSimpleName(), ex.getMessage()));
                    queueEntry.setAttempts(queueEntry.getAttempts() + 1);

                    if (queueEntry.getAttempts() > MESSAGE_MAX_ATTEMPTS) {
                        message.setStatus(MessageStatus.ERROR);
                        messageRepository.save(message);
                        messageQueueRepository.delete(queueEntry);
                    } else {
                        message.setStatus(MessageStatus.SENDING);
                        messageRepository.save(message);
                        queueEntry.setLastAttempt(OffsetDateTime.now());
                        queueEntry.setLastError(message.getError());
                        messageQueueRepository.save(queueEntry);
                    }
                }
            }
        }
    }

    protected abstract void sendMessage(T message);

    protected abstract R createQueueEntry();
}
