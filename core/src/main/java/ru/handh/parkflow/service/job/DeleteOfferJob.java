package ru.handh.parkflow.service.job;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import ru.handh.parkflow.db.model.exception.SchedulerTasksException;
import ru.handh.parkflow.service.ParkPlaceService;

public class DeleteOfferJob extends QuartzJobBean {

    @Autowired
    private ParkPlaceService parkPlaceService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        try {
            parkPlaceService.deleteExpiredOffer(context.getMergedJobDataMap().getLongFromString("pp_id"));
        } catch (NotFoundException | SchedulerTasksException e) {
            throw new JobExecutionException(e.getMessage(), e);
        }
    }
}
