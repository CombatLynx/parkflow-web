package ru.handh.parkflow.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.db.model.feedback.FeedbackReasonType;
import ru.handh.parkflow.db.model.FeedbackEntity;
import ru.handh.parkflow.db.repository.FeedbackRepository;
import ru.handh.parkflow.dto.FeedbackDto;
import ru.handh.parkflow.dto.FeedbackReasonDto;
import ru.handh.parkflow.dto.response.BaseResponse;
import ru.handh.parkflow.dto.response.GetFeedbackReasonResponse;
import ru.handh.parkflow.dto.response.GetFeedbackResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FeedbackService {

    private final MessageSource messageSource;
    private final FeedbackRepository feedbackRepository;

    @Transactional
    public ResponseEntity<BaseResponse> sendFeedback(String reason, String message) {
        FeedbackEntity feedbackEntity = new FeedbackEntity();
        feedbackEntity.setReason(reason);
        feedbackEntity.setMessage(message);

        feedbackRepository.save(feedbackEntity);
        return new ResponseEntity<>(new BaseResponse(), HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<GetFeedbackResponse> getFeedback() {

        List<FeedbackEntity> feedback = feedbackRepository.findAll();

        return new ResponseEntity<>(
                new GetFeedbackResponse(feedback
                        .stream()
                        .map(entity -> new FeedbackDto(entity))
                        .collect(Collectors.toList())),
                HttpStatus.OK
        );
    }

    @Transactional
    public ResponseEntity<GetFeedbackReasonResponse> getFeedbackReason() {
        List<FeedbackReasonDto> feedbackReason = new ArrayList<>();
        Arrays.stream(FeedbackReasonType.values()).forEach(reason ->
                feedbackReason.add(new FeedbackReasonDto(
                        reason,
                        messageSource.getMessage(reason.getType(), null, LocaleContextHolder.getLocale())))
        );
        return new ResponseEntity<>(new GetFeedbackReasonResponse(feedbackReason), HttpStatus.OK);
    }

}
