package ru.handh.parkflow.service;

import org.springframework.stereotype.Service;

@Service
public class HealthService {

    public String message() {
        return "Server started....";
    }
}
