package ru.handh.parkflow.service.job;

public class ParkPlaceTriggerGroupGenerator {
    public static String groupOf(Long parkPlaceId) {
        return "pp_".concat(parkPlaceId.toString());
    }
}
