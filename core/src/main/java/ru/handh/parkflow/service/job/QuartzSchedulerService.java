package ru.handh.parkflow.service.job;

import lombok.RequiredArgsConstructor;
import org.quartz.*;
import org.springframework.context.ApplicationContext;

import java.util.Date;

import static org.quartz.TriggerBuilder.newTrigger;

@RequiredArgsConstructor
public class QuartzSchedulerService {

    protected final ApplicationContext ctx;
    protected final Scheduler scheduler;

    public JobDetail getJobDetailByName(String jobBeanName) {
        return (JobDetail) ctx.getBean(jobBeanName);
    }

    public Trigger createDateTriggerForJob(JobDetail job, String triggerName, String triggerGroup, Date start, JobDataMap dataMap) {
        return buildByData(
                newTrigger().forJob(job)
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                .withMisfireHandlingInstructionIgnoreMisfires())
                        .startAt(start)
                        .withIdentity(triggerName, triggerGroup),
                dataMap);
    }

    public void scheduleJob(String jobBeanName, Trigger trigger) throws SchedulerException {
        if (scheduler.checkExists(JobKey.jobKey(jobBeanName))) {
            scheduler.scheduleJob(trigger);
        } else {
            scheduler.scheduleJob(getJobDetailByName(jobBeanName), trigger);
        }
    }

    public void unscheduleTriggerJob(TriggerKey triggerKey) throws SchedulerException {
        if (scheduler.checkExists(triggerKey)) {
            scheduler.unscheduleJob(triggerKey);
        }
    }

    private Trigger buildByData(TriggerBuilder triggerBuilder, JobDataMap dataMap) {
        if (dataMap != null) {
            return triggerBuilder.usingJobData(dataMap).build();
        }
        return triggerBuilder.build();
    }
}
