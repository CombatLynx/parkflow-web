package ru.handh.parkflow.service;

import com.google.i18n.phonenumbers.NumberParseException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.db.model.auth.AuthInfoEntity;
import ru.handh.parkflow.db.model.exception.*;
import ru.handh.parkflow.db.model.user.CarColorType;
import ru.handh.parkflow.db.model.user.CarEntity;
import ru.handh.parkflow.db.model.user.CountryEntity;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.db.repository.*;
import ru.handh.parkflow.dto.AccountStateDto;
import ru.handh.parkflow.dto.CarDto;
import ru.handh.parkflow.dto.UserInfoDto;
import ru.handh.parkflow.dto.CarColorDto;
import ru.handh.parkflow.dto.response.GetCarColorsResponse;
import ru.handh.parkflow.dto.response.GetUserInfoResponse;
import ru.handh.parkflow.util.UserValidatorUtil;

import java.util.*;

@Service
@RequiredArgsConstructor
public class UserService {

    private final MessageSource messageSource;
    private final ApprovePhoneCodeService approvePhoneCodeService;
    private final UserRepository userRepository;
    private final AuthInfoRepository authInfoRepository;
    private final UserLanguageRepository userLanguageRepository;
    private final CountryRepository countryRepository;
    private final CarRepository carRepository;

    @Transactional
    public UserEntity registerUserByPhone(String phone) throws NumberParseException, WrongArgumentException, DuplicatePhoneException {
        UserValidatorUtil.validatePhone(phone, messageSource);
        if(userRepository.findByPhone(phone).isPresent()) {
            throw new DuplicatePhoneException(messageSource.getMessage(
                    "user.duplicate_phone",
                    null,
                    LocaleContextHolder.getLocale()
            ));
        }

        UserEntity userEntity = new UserEntity();
        userEntity.setPhone(phone);
        userEntity.setActive(true);
        userEntity.setHavePermit(false);
        userRepository.save(userEntity);
        createApprovedAuthByPhone(userEntity);
        //TODO:
        //balanceService.createUserBalance(userEntity.getId());
        return userEntity;
    }

    @Transactional
    public AccountStateDto getAccountState(Long userId) throws EntityNotFoundException {
        AccountStateDto accountStateDTO = new AccountStateDto();
        UserInfoDto userInfoDto = getUserById(userId);
        if (validateUserInfo(userInfoDto).size() > 0) {
            accountStateDTO.setFillUserInfoRequired(true);
        }
        List<AuthInfoEntity> authInfos = authInfoRepository.findByUserId(userId);
        authInfos.forEach(authInfo -> {
            if (!authInfo.getApproved()) {
                accountStateDTO.setConfirmPhoneRequired(true);
            }
        });
        return accountStateDTO;
    }

    @Transactional
    public UserInfoDto getUserById(Long id) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(
                messageSource.getMessage("user.not_found", null, LocaleContextHolder.getLocale()))
        );



        UserInfoDto.UserInfoDtoBuilder userInfoDtoBuilder = UserInfoDto.builder()
                .id(userEntity.getId())
                .email(userEntity.getEmail())
                .phone(userEntity.getPhone())
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .active(userEntity.isActive())
                .havePermit(userEntity.isHavePermit());
        if (userEntity.getLanguage() != null) {
            userInfoDtoBuilder.userLanguage(userEntity.getLanguage().getLanguage());
        }
        if (userEntity.getCountry() != null) {
            userInfoDtoBuilder.country(messageSource.getMessage(
                    userEntity.getCountry().getCountry().toLowerCase(),
                    null,
                    LocaleContextHolder.getLocale()
            ));
        }

        carRepository.findByUserId(id).ifPresent(car -> userInfoDtoBuilder
                .car(CarDto.builder()
                        .model(car.getModel())
                        .make(car.getMake())
                        .number(car.getNumber())
                        .color(Optional.ofNullable(car.getColor())
                                .map(color ->
                                        new CarColorDto(color, messageSource.getMessage(
                                                color.getType(),
                                                null,
                                                LocaleContextHolder.getLocale())))
                                .orElse(null))
                        .build())
//                .carNumber(car.getNumber())
//                .carModel(car.getModel())
//                .carMake(car.getMake())
//                .carColor(Optional.ofNullable(car.getColor())
//                        .map(color ->
//                                new CarColorDto(color, messageSource.getMessage(
//                                        color.getType(),
//                                        null,
//                                        LocaleContextHolder.getLocale())))
//                        .orElse(null)
//                )
                );

        return userInfoDtoBuilder.build();
    }

    //TODO:
    @Transactional
    public ResponseEntity<GetUserInfoResponse> getOtherUserById(Long mainUserId, Long otherUserId, Long offerId) throws EntityNotFoundException {
        UserInfoDto otherUser = getUserById(otherUserId);
        if (mainUserId.equals(otherUserId)) {
            return new ResponseEntity<>(new GetUserInfoResponse(otherUser), HttpStatus.OK);
        }
        otherUser.setEmail(null);
        otherUser.setUserLanguage(null);
        otherUser.setCountry(null);
        otherUser.setLastName(null);
        /*ParkPlaceEntity parkPlace = null;
        if (offerId != null) {
            try {
                Optional<ParkPlaceEntity> o = parkPlaceService.getAvailableParkPlaces(mainUserId, otherUserId).stream()
                        .filter(pp -> pp.getId().equals(offerId)).findFirst();
                if (o.isPresent()) {
                    parkPlace = o.get();
                }
            } catch (Exception e) {
                parkPlace = null;
            }
        }
        if (parkPlace == null) {*/
            otherUser.setPhone(null);
            otherUser.setCar(null);

        /*} else {
            Integer timeBorderMins = Integer.valueOf(propertyService.getByName(
                    PropertyName.PHONE_HIDE_TIME_BORDER_MINS.name()).getValue());
            //if now isn't in borders of showing phones
            if (OffsetDateTime.now().isBefore(parkPlace.getClaimUserStart().minus(timeBorderMins, ChronoUnit.MINUTES)) ||
                    OffsetDateTime.now().isAfter(parkPlace.getClaimUserStart().plus(timeBorderMins, ChronoUnit.MINUTES))) {
                otherUser.setPhone(null);
            }
        }*/
        return new ResponseEntity<>(new GetUserInfoResponse(otherUser), HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<GetCarColorsResponse> getCarColors() {
        List<CarColorDto> carColors = new ArrayList<>();
        Arrays.stream(CarColorType.values()).forEach(color ->
                carColors.add(new CarColorDto(
                        color,
                        messageSource.getMessage(color.getType(), null, LocaleContextHolder.getLocale())))
        );
        return new ResponseEntity<>(new GetCarColorsResponse(carColors), HttpStatus.OK);
    }

    @Transactional
    public void updateUser(Long userId, String firstName, String lastName, String email, CarDto car) throws WrongArgumentException {
        updateFirstName(userId, firstName);
        updateLastName(userId, lastName);
        updateEmail(userId, email);
        updateCar(userId, car);
    }

    @Transactional
    public void updateFirstName(Long userId, String firstName) {
        UserEntity userEntity = userRepository.getOne(userId);
        userEntity.setFirstName(firstName);
        userRepository.save(userEntity);
    }

    @Transactional
    public void updateLastName(Long userId, String lastName) {
        UserEntity userEntity = userRepository.getOne(userId);
        userEntity.setLastName(lastName);
        userRepository.save(userEntity);
    }

    @Transactional
    public void updateHavePermit(Long userId, boolean havePermit) {
        UserEntity userEntity = userRepository.getOne(userId);
        userEntity.setHavePermit(havePermit);
        userRepository.save(userEntity);
    }

    @Transactional
    public void updatePhoneRequest(String phone)
            throws NumberParseException, WrongArgumentException, ApproveCodeException, UserException, DuplicatePhoneException {

        if(userRepository.findByPhone(phone).isPresent()) {
            throw new DuplicatePhoneException(messageSource.getMessage(
                    "user.duplicate_phone",
                    null,
                    LocaleContextHolder.getLocale()
            ));
        }
        approvePhoneCodeService.createApprovePhoneCode(phone);
    }

    @Transactional
    public void updatePhoneConfirm(Long userId, String phone, String code)
            throws NumberParseException, WrongArgumentException, ApproveCodeException, EntityNotFoundException {

        approvePhoneCodeService.checkApprovePhoneCode(phone, code);
        UserEntity userEntity = userRepository.getOne(userId);
        userEntity.setPhone(phone);
        userRepository.save(userEntity);
    }

    @Transactional
    public void updateEmail(Long userId, String email) throws WrongArgumentException {
        UserValidatorUtil.validateEmail(email, messageSource);
        UserEntity userEntity = userRepository.getOne(userId);
        userEntity.setEmail(email);
        userRepository.save(userEntity);
    }

    @Transactional
    public void updateCountry(Long userId, Long countryId) throws EntityNotFoundException {
        CountryEntity country = countryRepository.findById(countryId)
                .orElseThrow(() -> new EntityNotFoundException(
                messageSource.getMessage("country.not_found", null, LocaleContextHolder.getLocale()))
        );

        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(
                messageSource.getMessage("user.not_found", null, LocaleContextHolder.getLocale()))
        );
        userEntity.setCountry(country);
        userRepository.save(userEntity);
    }

    @Transactional
    public void updateCar(Long userId, CarDto carDto) {
        UserEntity userEntity = userRepository.getOne(userId);
        CarEntity carEntity = userEntity.getCar();
        if (carEntity == null) {
            carEntity = new CarEntity();
            carEntity.setUser(userEntity);
        }
        carEntity.setColor(carDto.getColor().getId());
        carEntity.setModel(carDto.getModel());
        carEntity.setMake(carDto.getMake());
        carEntity.setNumber(carDto.getNumber());

        carRepository.save(carEntity);
    }

    @Transactional
    public void savePush(Long userId, String platform, String pushToken) {
        UserEntity user = userRepository.getOne(userId);
        user.setPushToken(pushToken);
        user.setPlatform(platform);
        userRepository.save(user);
    }

    @Transactional
    public void deletePush(Long userId) {
        UserEntity user = userRepository.getOne(userId);
        user.setPushToken(null);
        user.setPlatform(null);
        userRepository.save(user);
    }

    private void createApprovedAuthByPhone(UserEntity userEntity) {
        AuthInfoEntity authInfo = new AuthInfoEntity();
        authInfo.setUser(userEntity);
        authInfo.setApproved(true);
        authInfoRepository.save(authInfo);
    }

    private Map<String, Integer> validateUserInfo(UserInfoDto userInfoDTO) {
        Map<String, Integer> errors = new HashMap<>();
        if (userInfoDTO.getPhone() == null) {
            errors.put("phone", 400);
        } else {
            try {
                UserValidatorUtil.validatePhone(userInfoDTO.getPhone(), messageSource);
                userRepository.findByPhone(userInfoDTO.getPhone()).ifPresent(entity -> {
                    if (!entity.getId().equals(userInfoDTO.getId())) {
                        errors.put("phone", 409);
                    }
                });
            } catch (WrongArgumentException | NumberParseException e) {
                errors.put("phone", 400);
            }
        }
        if (userInfoDTO.getFirstName() == null || userInfoDTO.getFirstName().isEmpty()) {
            errors.put("firstName", 400);
        }
        if (userInfoDTO.getCar() == null) {
            errors.put("car", 400);
        } else {

            if (userInfoDTO.getCar().getNumber() == null || userInfoDTO.getCar().getNumber().isEmpty()) {
                errors.put("carNumber", 400);
            }
            if (userInfoDTO.getCar().getColor() == null) {
                errors.put("carColor", 400);
            }
            if (userInfoDTO.getCar().getModel() == null || userInfoDTO.getCar().getModel().isEmpty()) {
                errors.put("carModel", 400);
            }
        }
        if ((userInfoDTO.getUserLanguage() == null) ||
                (userLanguageRepository.findByLanguage(userInfoDTO.getUserLanguage()) == null)) {
            errors.put("userLanguage", 400);
        }
        if ((userInfoDTO.getCountry() == null) ||
                (countryRepository.findByCountry(userInfoDTO.getCountry()) == null)) {
            errors.put("country", 400);
        }
        return errors;
    }
}
