package ru.handh.parkflow.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.db.model.exception.EntityNotFoundException;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.db.model.user.UserPositionEntity;
import ru.handh.parkflow.db.repository.PositionRepository;
import ru.handh.parkflow.db.repository.UserRepository;
import ru.handh.parkflow.dto.CoordinateDto;

@Transactional
@Service
@RequiredArgsConstructor
public class PositionService {

    private final MessageSource messageSource;
    private final UserRepository userRepository;
    private final PositionRepository positionRepository;

    public CoordinateDto getPosition(Long userId) throws EntityNotFoundException {
        return new CoordinateDto(userRepository.findById(userId).map(UserEntity::getUserPosition).orElseThrow(() ->
                new ru.handh.parkflow.db.model.exception.EntityNotFoundException(messageSource.getMessage(
                        "park_place.position_not_found",
                        null,
                        LocaleContextHolder.getLocale()))
        ));
    }

    public ResponseEntity<CoordinateDto> updatePosition(Long userId, CoordinateDto coordinateDto) {
        UserEntity user = userRepository.getOne(userId);

        UserPositionEntity newPosition = UserPositionEntity.builder()
                .lat(coordinateDto.getLat())
                .lon(coordinateDto.getLon())
                .accuracy(coordinateDto.getAccuracy())
                .user(user)
                .build();

        positionRepository.save(newPosition);

        user.setUserPosition(newPosition);
        userRepository.save(user);

        return new ResponseEntity<>(new CoordinateDto(newPosition), HttpStatus.OK);
    }
}
