package ru.handh.parkflow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.handh.parkflow.db.model.parking.ParkPlaceStatus;
import ru.handh.parkflow.db.repository.ParkPlaceRepository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
public class ParkPlaceLockService {

    private Map<Long, ReentrantReadWriteLock> locks;
    private ParkPlaceRepository parkPlaceRepository;
    private ReentrantReadWriteLock readWriteLock;

    @PostConstruct
    private void init() {
        this.readWriteLock = new ReentrantReadWriteLock();
        this.locks = new HashMap<>();
        parkPlaceRepository.findAll()
                .forEach(pp -> {
                    if (!(pp.getStatus().equals(ParkPlaceStatus.CANCELED)
                            || pp.getStatus().equals(ParkPlaceStatus.FINISHED))) {
                        locks.put(pp.getId(), new ReentrantReadWriteLock());
                    }
                });
    }

    public ReentrantReadWriteLock getLock(Long parkPlaceId) {
        ReentrantReadWriteLock lock = locks.get(parkPlaceId);
        if (lock != null) {
            return lock;
        }
        return readWriteLock;
    }

    public ReentrantReadWriteLock createLock(Long parkPlaceId) {
        return locks.put(parkPlaceId, new ReentrantReadWriteLock());
    }

    public void deleteLock(Long parkPlaceId) {
        locks.remove(parkPlaceId);
    }

    @Autowired
    public void setParkPlaceRepository(ParkPlaceRepository parkPlaceRepository) {
        this.parkPlaceRepository = parkPlaceRepository;
    }
}
