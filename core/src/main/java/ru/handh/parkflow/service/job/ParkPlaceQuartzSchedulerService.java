package ru.handh.parkflow.service.job;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.context.ApplicationContext;
import ru.handh.parkflow.db.model.parking.ParkPlaceEntity;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ParkPlaceQuartzSchedulerService extends QuartzSchedulerService {

    public ParkPlaceQuartzSchedulerService(ApplicationContext ctx, Scheduler scheduler) {
        super(ctx, scheduler);
    }

    public List<Trigger> getConnectedTasks(ParkPlaceEntity parkPlace) throws SchedulerException {
        Set<TriggerKey> keys = this.scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(
                ParkPlaceTriggerGroupGenerator.groupOf(parkPlace.getId())));
        List<Trigger> triggers = new LinkedList<>();
        for (TriggerKey key : keys) {
            triggers.add(this.scheduler.getTrigger(key));
        }
        return triggers;
    }

    public void unscheduleConnectedTasks(ParkPlaceEntity parkPlace) throws SchedulerException {
        List<Trigger> triggers = getConnectedTasks(parkPlace);

        for (Trigger t : triggers) {
            unscheduleTriggerJob(t.getKey());
        }
    }
}
