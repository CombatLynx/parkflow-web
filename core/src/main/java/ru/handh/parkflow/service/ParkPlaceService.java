package ru.handh.parkflow.service;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.quartz.JobDataMap;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.handh.parkflow.config.ParkPlaceSchedulingConfig;
import ru.handh.parkflow.db.model.parking.ParkPlaceEntity;
import ru.handh.parkflow.db.model.parking.ParkPlaceStatus;
import ru.handh.parkflow.db.model.parking.ParkPlaceType;
import ru.handh.parkflow.db.model.exception.*;
import ru.handh.parkflow.db.model.property.PropertyName;
import ru.handh.parkflow.db.model.user.UserEntity;
import ru.handh.parkflow.db.repository.CarRepository;
import ru.handh.parkflow.db.repository.ParkPlaceRepository;
import ru.handh.parkflow.db.repository.UserRepository;
import ru.handh.parkflow.dto.CoordinateDto;
import ru.handh.parkflow.dto.LeavingDelayDto;
import ru.handh.parkflow.dto.ParkPlaceDto;
import ru.handh.parkflow.dto.RouteDto;
import ru.handh.parkflow.dto.SearchParkPlaceDto;
import ru.handh.parkflow.dto.request.CreateParkPlaceRequest;
import ru.handh.parkflow.dto.request.SearchParkPlaceRequest;
import ru.handh.parkflow.dto.response.GetLeavingDelaysResponse;
import ru.handh.parkflow.dto.response.GetRouteResponse;
import ru.handh.parkflow.dto.response.ParkPlaceResponse;
import ru.handh.parkflow.dto.response.SearchParkPlaceResponse;
import ru.handh.parkflow.mapper.ParkPlaceDtoMapper;
import ru.handh.parkflow.service.job.ParkPlaceJobType;
import ru.handh.parkflow.service.job.ParkPlaceQuartzSchedulerService;
import ru.handh.parkflow.service.job.ParkPlaceTriggerGroupGenerator;
import ru.handh.parkflow.util.PositionUtil;
import ru.handh.parkflow.util.TimeUtil;

import java.util.ArrayList;
import java.util.List;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ParkPlaceService {

    @Qualifier(value = "parkPlaceQuartzSchedulerService")
    private final ParkPlaceQuartzSchedulerService schedulerService;
    private final MessageSource messageSource;
    private final ParkPlaceDtoMapper parkPlaceDtoMapper;
    private final PositionUtil positionUtil;
    private final PositionService positionService;
    private final PropertyService propertyService;
    private final ParkPlaceLockService parkPlaceLockService;
    private final RouteService routeService;
    private final ParkPlaceRepository parkPlaceRepository;
    private final CarRepository carRepository;
    private final UserRepository userRepository;

    private final static int DEFAULT_ACCURACY = 100;

    @Transactional
    public ResponseEntity<GetLeavingDelaysResponse> getLeavingDelays() {
        List<LeavingDelayDto> delays = new ArrayList<>();
        delays.add(new LeavingDelayDto(1L, Long.valueOf(propertyService.getByName(PropertyName.LEAVING_DELAY_FIRST.name()).getValue())));
        delays.add(new LeavingDelayDto(2L, Long.valueOf(propertyService.getByName(PropertyName.LEAVING_DELAY_SECOND.name()).getValue())));
        delays.add(new LeavingDelayDto(3L, Long.valueOf(propertyService.getByName(PropertyName.LEAVING_DELAY_THIRD.name()).getValue())));
        delays.add(new LeavingDelayDto(4L, Long.valueOf(propertyService.getByName(PropertyName.LEAVING_DELAY_FORTH.name()).getValue())));

        return new ResponseEntity<>(new GetLeavingDelaysResponse(delays), HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<ParkPlaceResponse> createParkPlace(CreateParkPlaceRequest request, UserEntity user)
            throws EntityNotFoundException, SchedulerTasksException, WrongPositionException, WrongArgumentException {
        //TODO: balance, email, job for delete
        carRepository.findByUserId(user.getId()).orElseThrow(() -> new EntityNotFoundException(messageSource.getMessage(
                "car.not_found",
                null,
                LocaleContextHolder.getLocale()
        )));
        CoordinateDto coordinateDto = positionService.getPosition(user.getId());
        if (!positionUtil.rightPosition(coordinateDto, request.getLat(), request.getLon())) {
            throw new WrongPositionException(
                    messageSource.getMessage("park_place.wrong_position", null, LocaleContextHolder.getLocale()));
        }

        ParkPlaceEntity parkPlaceEntity = ParkPlaceEntity.builder()
                .status(ParkPlaceStatus.LOOKING)
                .lat(request.getLat())
                .lon(request.getLon())
                .createTime(TimeUtil.ofEpoch(request.getCreateTime()))
                .leavingDelay(request.getLeavingDelay())
                .offerUser(user)
                .type(ParkPlaceType.valueOf(request.getType()))
                .build();
        parkPlaceRepository.save(parkPlaceEntity);

        JobDataMap jdm = new JobDataMap();
        jdm.putAsString("pp_id", parkPlaceEntity.getId());

        try {
            schedulerService.scheduleJob(
                    ParkPlaceSchedulingConfig.DELETE_OFFER_JOB_NAME,
                    schedulerService.createDateTriggerForJob(
                            schedulerService.getJobDetailByName(ParkPlaceSchedulingConfig.DELETE_OFFER_JOB_NAME),
                            ParkPlaceJobType.DELETE.name(),
                            ParkPlaceTriggerGroupGenerator.groupOf(parkPlaceEntity.getId()),
                            //TODO: запускать немного позже, чем пользователь должен уехать?
                            Date.from(parkPlaceEntity.getCreateTime().toInstant().plus(parkPlaceEntity.getLeavingDelay(), ChronoUnit.MILLIS)),
                            jdm
                    )
            );
        } catch (SchedulerException ex) {
            throw new SchedulerTasksException(messageSource.getMessage(
                    "park_place.error_planning_delete",
                    new Object[]{parkPlaceEntity.getId()},
                    LocaleContextHolder.getLocale()
            ));
        }

        return new ResponseEntity<>(new ParkPlaceResponse(parkPlaceDtoMapper.convertToDto(parkPlaceEntity)), HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<SearchParkPlaceResponse> searchParkPlaces(SearchParkPlaceRequest request) throws RouteException {
        List<ParkPlaceDto> parkPlacesInRadius = parkPlaceRepository.getInRadius(request.getLat(), request.getLon(), request.getRadius())
                .stream()
                .map(parkPlaceDtoMapper::convertToDto)
                .collect(Collectors.toList());

        List<SearchParkPlaceDto> parkPlaces = routeService.getNearestParkPlaces(request.getLat(), request.getLon(), parkPlacesInRadius);

        return new ResponseEntity<>(new SearchParkPlaceResponse(parkPlaces), HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<ParkPlaceResponse> claimOffer(Long userId, Long offerId, Long startDate) throws EntityNotFoundException, NotAllowedException, SchedulerTasksException {
        ReentrantReadWriteLock.WriteLock writeLock = parkPlaceLockService.getLock(offerId).writeLock();
        writeLock.lock();
        try {
            UserEntity user = userRepository.findById(userId).orElseThrow(() ->
                    new EntityNotFoundException(messageSource.getMessage(
                            "user.not_found",
                            null,
                            LocaleContextHolder.getLocale()))
            );

            ParkPlaceDto dto = parkPlaceDtoMapper.convertToDto(parkPlaceRepository.findById(offerId).orElseThrow(() ->
                    new EntityNotFoundException(messageSource.getMessage(
                            "park_place.not_found",
                            null,
                            LocaleContextHolder.getLocale()))
            ));
            if (dto.getOfferUserInfo().getId().equals(userId)) {
                throw new NotAllowedException(
                        messageSource.getMessage("park_place.can_not_claim_yours", null, LocaleContextHolder.getLocale()));
            }
            ParkPlaceEntity parkPlaceEntity = checkAndReserveParkPlace(offerId, userId);
           //TODO: timeDelay (from property), balance, send push to claimer?, scheduleRemindPush?

            parkPlaceEntity.setClaimUser(user);
            parkPlaceRepository.save(parkPlaceEntity);

            try {
                schedulerService.unscheduleTriggerJob(TriggerKey.triggerKey(
                        ParkPlaceJobType.DELETE.name(),
                        ParkPlaceTriggerGroupGenerator.groupOf(parkPlaceEntity.getId())
                ));
            } catch (SchedulerException ex) {
                throw new SchedulerTasksException(
                        messageSource.getMessage(
                                "park_place.error_cleanup_auto_delete",
                                new Object[]{parkPlaceEntity.getId()},
                                LocaleContextHolder.getLocale())
                );
            }
            return new ResponseEntity<>(new ParkPlaceResponse(parkPlaceDtoMapper.convertToDto(parkPlaceRepository.getOne(offerId))), HttpStatus.OK);
        } finally {
            writeLock.unlock();
        }
    }

    @Transactional
    public ResponseEntity<GetRouteResponse> getRoute(Long offerId, Double startLat, Double startLon) throws EntityNotFoundException, RouteException {
        ParkPlaceEntity parkPlace = parkPlaceRepository.findById(offerId).orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage(
                        "park_place.not_found",
                        null,
                        LocaleContextHolder.getLocale()))
        );
        RouteDto route = routeService.getRoute(startLat, startLon, parkPlace.getLat(), parkPlace.getLon());
        return new ResponseEntity<>(new GetRouteResponse(route), HttpStatus.OK);
    }

    @Transactional
    public void deleteExpiredOffer(Long parkId) throws NotFoundException, SchedulerTasksException {
        deleteOffer(parkId);
        ReentrantReadWriteLock.WriteLock writeLock = parkPlaceLockService.getLock(parkId).writeLock();
        writeLock.lock();
        try {
            ParkPlaceEntity parkPlace = parkPlaceRepository.findById(parkId).orElseThrow(() ->
                    new EntityNotFoundException(messageSource.getMessage(
                            "park_place.not_found",
                            null,
                            LocaleContextHolder.getLocale()))
            );
            if(parkPlace.getStatus().equals(ParkPlaceStatus.CANCELED)) {
                parkPlace.setStatus(ParkPlaceStatus.EXPIRED);
            }
            parkPlaceRepository.save(parkPlace);
        } finally {
            writeLock.unlock();
        }
    }

    @Transactional
    public void deleteOffer(Long parkId) throws NotFoundException, SchedulerTasksException {
        ReentrantReadWriteLock.WriteLock writeLock = parkPlaceLockService.getLock(parkId).writeLock();
        writeLock.lock();
        try {
            ParkPlaceEntity parkPlace = parkPlaceRepository.findById(parkId).orElseThrow(() ->
                    new EntityNotFoundException(messageSource.getMessage(
                            "park_place.not_found",
                            null,
                            LocaleContextHolder.getLocale()))
            );
            checkParkPlaceStatus(parkPlace, ParkPlaceStatus.LOOKING);

            try {
                schedulerService.unscheduleConnectedTasks(parkPlace);
            } catch (SchedulerException ex) {
                throw new SchedulerTasksException(messageSource.getMessage(
                        "park_place.error_cleanup_tasks",
                        new Object[]{parkPlace.getId()},
                        LocaleContextHolder.getLocale()
                ));
            }

            parkPlace.setStatus(ParkPlaceStatus.CANCELED);
            parkPlaceRepository.save(parkPlace);

            //TODO: push
        } finally {
            writeLock.unlock();
        }
    }

    @Transactional
    public void approveOffer(Long userId, Long offerId) throws NotAllowedException {
        ReentrantReadWriteLock.WriteLock writeLock = parkPlaceLockService.getLock(offerId).writeLock();
        writeLock.lock();
        try {
        ParkPlaceEntity parkPlace = parkPlaceRepository.getOne(offerId);
        if (!parkPlace.getClaimUser().getId().equals(userId)) {
            throw new NotAllowedException(
                    messageSource.getMessage("park_place.forbidden", null, LocaleContextHolder.getLocale()));
        }
        //TODO: block, balance, send email and push

        parkPlace.setStatus(ParkPlaceStatus.FINISHED);
        parkPlaceRepository.save(parkPlace);

        CoordinateDto coordinateDto = CoordinateDto.builder()
                .lat(parkPlace.getLat())
                .lon(parkPlace.getLon())
                .accuracy(DEFAULT_ACCURACY)
                .build();
        positionService.updatePosition(parkPlace.getClaimUser().getId(), coordinateDto);
        } finally {
            writeLock.unlock();
        }
    }

    @Transactional
    public void cancel(Long userId, Long parkPlaceId) throws EntityNotFoundException, WrongArgumentException, SchedulerTasksException {
        ReentrantReadWriteLock.WriteLock writeLock = parkPlaceLockService.getLock(parkPlaceId).writeLock();
        writeLock.lock();
        try {
            ParkPlaceEntity parkPlaceEntity = parkPlaceRepository.findById(parkPlaceId).orElseThrow(() ->
                    new EntityNotFoundException(messageSource.getMessage(
                        "park_place.not_found",
                        null,
                        LocaleContextHolder.getLocale()))
            );
            if ((parkPlaceEntity.getStatus() != ParkPlaceStatus.ACTIVE) && (parkPlaceEntity.getStatus() != ParkPlaceStatus.LOOKING)) {
                throw new WrongArgumentException(
                        messageSource.getMessage("park_place.can_not_cancel_in_state", null, LocaleContextHolder.getLocale()));
            }
            if (parkPlaceEntity.getOfferUser().getId().equals(userId)) {
                cancelAsOfferer(parkPlaceEntity);
            } else {
                Optional<UserEntity> claimer = Optional.ofNullable(parkPlaceEntity.getClaimUser());
                if(claimer.isPresent() && claimer.get().getId().equals(userId)) {
                    cancelAsClaimer(parkPlaceEntity);
                } else {
                    throw new WrongArgumentException(
                            messageSource.getMessage("park_place.user_not_bound", null, LocaleContextHolder.getLocale()));
                }
            }
            try {
                schedulerService.unscheduleConnectedTasks(parkPlaceEntity);
            } catch (SchedulerException ex) {
                throw new SchedulerTasksException(messageSource.getMessage(
                        "park_place.error_shifting_tasks",
                        new Object[]{parkPlaceEntity.getId()},
                        LocaleContextHolder.getLocale()
                ));
            }
        } finally {
            writeLock.unlock();
        }
    }

    @Transactional
    public void cancelAsOfferer(ParkPlaceEntity parkPlace) {
        //TODO: push, block, balance

        if (parkPlace.getClaimUser() == null) {
           //TODO
        } else {
            //TODO
        }
        parkPlace.setStatus(ParkPlaceStatus.CANCELED);
        parkPlaceRepository.save(parkPlace);
    }

    @Transactional
    public void cancelAsClaimer(ParkPlaceEntity parkPlace) {
        //TODO: push, balance

        parkPlace.setStatus(ParkPlaceStatus.CANCELED);
        parkPlaceRepository.save(parkPlace);
    }

    private synchronized ParkPlaceEntity checkAndReserveParkPlace(Long offerId, Long userId) throws NotAllowedException, EntityNotFoundException {
        ParkPlaceEntity parkPlaceEntity = parkPlaceRepository.findById(offerId).orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage(
                        "park_place.not_found",
                        null,
                        LocaleContextHolder.getLocale()))
        );
        if (parkPlaceEntity.getStatus() != ParkPlaceStatus.LOOKING) {
            throw new NotAllowedException(
                    messageSource.getMessage("park_place.already_reserved", null, LocaleContextHolder.getLocale()));
        }
        //TODO: check balance
        parkPlaceEntity.setStatus(ParkPlaceStatus.ACTIVE);
        parkPlaceRepository.save(parkPlaceEntity);
        return parkPlaceEntity;
    }

    public void checkParkPlaceStatus(ParkPlaceEntity parkPlace, ParkPlaceStatus... statuses) {
        for (ParkPlaceStatus s : statuses) {
            if (!parkPlace.getStatus().equals(s)) {
                throw new RuntimeException(
                        messageSource.getMessage(
                                "park_place.wrong_status",
                                new Object[]{parkPlace.getId(), s, parkPlace.getStatus()},
                                LocaleContextHolder.getLocale())
                );
            }
        }
    }
}
