package ru.handh.parkflow.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.handh.parkflow.db.repository.FaqRepository;
import ru.handh.parkflow.db.repository.HelpRepository;
import ru.handh.parkflow.db.repository.LegalRepository;
import ru.handh.parkflow.dto.FaqDto;
import ru.handh.parkflow.dto.HelpDto;
import ru.handh.parkflow.dto.LegalDto;
import ru.handh.parkflow.dto.PageWithTitleDto;
import ru.handh.parkflow.dto.response.GetHelpAndFaqResponse;
import ru.handh.parkflow.dto.response.GetLegalResponse;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class HelpService {

    private final HelpRepository helpRepository;
    private final FaqRepository faqRepository;
    private final LegalRepository legalRepository;
    private final ModelMapper modelMapper;

    public ResponseEntity<GetHelpAndFaqResponse> getHelpAndFaq(Pageable pageable) {
        Page<HelpDto> help = helpRepository
                .findAll(pageable)
                .map(entity -> modelMapper.map(entity, HelpDto.class));

        Page<FaqDto> faq = faqRepository
                .findAll(pageable)
                .map(entity -> modelMapper.map(entity, FaqDto.class));

        List<PageWithTitleDto> list = new ArrayList<>();
        list.add(new PageWithTitleDto<>("Help", help));
        list.add(new PageWithTitleDto<>("FAQ", faq));

        return new ResponseEntity<>(new GetHelpAndFaqResponse(list), HttpStatus.OK);
    }

    public ResponseEntity<GetLegalResponse> getLegal(Pageable pageable) {
        return new ResponseEntity<>(
                new GetLegalResponse(legalRepository
                    .findAll(pageable)
                    .map(entity -> modelMapper.map(entity, LegalDto.class))),
                HttpStatus.OK);
    }

    public ResponseEntity<byte[]> getTermsOfService() throws IOException {
        String filename = "Parklook_Terms_of_Service_RUSSIAN.pdf";
        InputStream in = getClass().getResourceAsStream("/" + filename);
        byte[] contents = IOUtils.toByteArray(in);
        in.close();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=\""+filename+"\"");
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }
}
