package ru.handh.parkflow.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Getter
@PropertySource({"classpath:message_scheduling.properties"})
public class MessageSchedulingProperties {

    @Value("${parkflow.message.sms.period}")
    private Long SMSPeriod;

    @Value("${parkflow.message.sms.initDelay}")
    private Long SMSInitDelay;
}
