package ru.handh.parkflow.config;

import lombok.RequiredArgsConstructor;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.handh.parkflow.service.job.ParkPlaceQuartzSchedulerService;
import ru.handh.parkflow.service.job.DeleteOfferJob;

@Configuration
@RequiredArgsConstructor
@ComponentScan(basePackages = {"ru.handh.parkflow.service.job"})
public class ParkPlaceSchedulingConfig {

    public final static String DELETE_OFFER_JOB_NAME = "deleteOfferJob";

    private final ApplicationContext applicationContext;

    @Bean(name = "parkPlaceQuartzSchedulerService")
    public ParkPlaceQuartzSchedulerService quartzOfferSchedulerService(final Scheduler parkPlaceScheduler) {
        return new ParkPlaceQuartzSchedulerService(applicationContext, parkPlaceScheduler);
    }

    @Bean(name = DELETE_OFFER_JOB_NAME)
    public JobDetail deleteOfferJobDetail() {
        return JobBuilder.newJob().ofType(DeleteOfferJob.class)
                .storeDurably()
                .requestRecovery(true)
                .withIdentity(DELETE_OFFER_JOB_NAME)
                .build();
    }
}
