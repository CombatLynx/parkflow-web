package ru.handh.parkflow.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.handh.parkflow.service.message.SmsMessageService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
public class MessageConfig {

    private final MessageSchedulingProperties messageSchedulingProperties;

    @Bean(name = "messageScheduler")
    public ScheduledExecutorService messageScheduler(SmsMessageService smsMessageService) {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(smsMessageService::sendQueue,
                messageSchedulingProperties.getSMSInitDelay(),
                messageSchedulingProperties.getSMSPeriod(),
                TimeUnit.MILLISECONDS);

        return scheduler;
    }
}
