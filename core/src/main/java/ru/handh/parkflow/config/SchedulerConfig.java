package ru.handh.parkflow.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.quartz.SchedulerFactoryBeanCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
public class SchedulerConfig {

    private DataSource dataSource;
    private PlatformTransactionManager transactionManager;

    public SchedulerConfig(@Qualifier("quartzDataSource") final DataSource dataSource,
                           @Qualifier("quartzTransactionManager") final PlatformTransactionManager transactionManager)
    {
        this.dataSource = dataSource;
        this.transactionManager = transactionManager;
    }

    @Bean
    public SchedulerFactoryBeanCustomizer schedulerFactoryBeanCustomizer()
    {
        return bean ->
        {
            bean.setDataSource(dataSource);
            bean.setTransactionManager(transactionManager);
        };
    }
}
