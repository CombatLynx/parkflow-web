package ru.handh.parkflow.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class BaseResponse {

    private boolean success = true;
    private int resultCode = 0;
    private String resultMessage = "Успешно";

    public BaseResponse(boolean success, int resultCode, String resultMessage) {
        this.success = success;
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }
}
