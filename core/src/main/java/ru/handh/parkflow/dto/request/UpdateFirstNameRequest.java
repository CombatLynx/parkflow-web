package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class UpdateFirstNameRequest {

    @Size(min = 2, max = 100, message = "{invalid_first_name}")
    private String firstName;
}
