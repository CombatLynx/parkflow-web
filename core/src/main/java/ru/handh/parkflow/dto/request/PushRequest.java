package ru.handh.parkflow.dto.request;

import lombok.Data;
import javax.validation.constraints.NotEmpty;

@Data
public class PushRequest {

    @NotEmpty
    private String platform;

    @NotEmpty
    private String pushToken;
}
