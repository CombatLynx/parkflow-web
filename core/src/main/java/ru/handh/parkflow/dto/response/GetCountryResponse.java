package ru.handh.parkflow.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.dto.CountryDto;

import java.util.List;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class GetCountryResponse extends BaseResponse {

    private List<CountryDto> countries;
}


