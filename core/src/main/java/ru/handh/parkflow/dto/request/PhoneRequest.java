package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PhoneRequest {

    @NotEmpty(message = "{invalid_phone}")
    private String phone;
}
