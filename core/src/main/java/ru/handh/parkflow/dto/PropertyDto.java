package ru.handh.parkflow.dto;

import lombok.Data;
import ru.handh.parkflow.db.model.property.PropertyEntity;

@Data
public class PropertyDto {

    private String name;
    private String value;
    private String type;
    private Boolean editable;
    private String description;

    public PropertyDto(PropertyEntity propertyEntity) {
        this.name = propertyEntity.getName().name();
        this.value = propertyEntity.getValue();
        this.type = propertyEntity.getType().name();
        this.editable = propertyEntity.getEditable();
        this.description = propertyEntity.getDescription();
    }
}
