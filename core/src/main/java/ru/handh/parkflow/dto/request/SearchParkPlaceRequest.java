package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SearchParkPlaceRequest {

    @NotNull
    private Double lat;

    @NotNull
    private Double lon;

    @NotNull
    private Double radius;
}
