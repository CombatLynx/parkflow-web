package ru.handh.parkflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SearchParkPlaceDto {

    private ParkPlaceDto parkPlace;
    double duration;
    double distance;
}
