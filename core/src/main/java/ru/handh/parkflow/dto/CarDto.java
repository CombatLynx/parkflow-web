package ru.handh.parkflow.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class CarDto {

    @Size(min = 2, max = 100, message = "{invalid_car_model}")
    private String model;

    @Size(min = 1, max = 100, message = "{invalid_car_make}")
    private String make;

    @Size(min = 2, max = 100, message = "{invalid_car_number}")
    private String number;

    @NotNull
    private CarColorDto color;
}
