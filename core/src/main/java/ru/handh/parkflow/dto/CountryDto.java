package ru.handh.parkflow.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import ru.handh.parkflow.db.model.user.CountryEntity;

@Data
@NoArgsConstructor
public class CountryDto {

    private Long id;
    private String country;

    public CountryDto(CountryEntity entity, MessageSource messageSource) {

        this.id = entity.getId();
        try {
            this.country = messageSource.getMessage(entity.getCountry().toLowerCase(), null, LocaleContextHolder.getLocale());
        } catch(NoSuchMessageException e) {
            this.country = null;
        }
    }
}
