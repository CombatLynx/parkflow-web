package ru.handh.parkflow.dto.request;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class UpdateHavePermitRequest {
    @NotNull
    private Boolean havePermit;
}