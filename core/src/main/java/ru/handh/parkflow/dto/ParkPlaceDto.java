package ru.handh.parkflow.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ParkPlaceDto {

    private Long id;
    private String type;
    private Long offerUserId;
    private UserInfoDto offerUserInfo;
    private Long createTime;
    private Long leavingDelay;
    private Double lat;
    private Double lon;
    private Long claimUserId;
    private UserInfoDto claimUserInfo;
}
