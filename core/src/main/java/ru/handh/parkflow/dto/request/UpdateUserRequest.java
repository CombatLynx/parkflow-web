package ru.handh.parkflow.dto.request;

import lombok.Data;
import ru.handh.parkflow.dto.CarDto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UpdateUserRequest {

    @Size(min = 2, max = 100, message = "{invalid_first_name}")
    private String firstName;

    @Size(min = 1, max = 100, message = "{invalid_last_name}")
    private String lastName;

    @Size(min = 5, max = 100, message = "{invalid_email}")
    private String email;

    @NotNull
    private CarDto car;
}
