package ru.handh.parkflow.dto.request;

import lombok.Data;
import ru.handh.parkflow.dto.CountryDto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UpdateCountryRequest {

    @NotNull
    private CountryDto country;
}
