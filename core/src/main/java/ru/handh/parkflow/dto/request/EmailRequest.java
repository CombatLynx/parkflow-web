package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class EmailRequest {

    @Size(min = 5, max = 100, message = "{invalid_email}")
    private String email;
}
