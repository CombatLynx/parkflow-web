package ru.handh.parkflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.parkflow.db.model.user.CarColorType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarColorDto {

    private CarColorType id;
    private String title;
}
