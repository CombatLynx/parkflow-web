package ru.handh.parkflow.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.dto.UserInfoDto;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class GetUserInfoResponse extends BaseResponse {

    private UserInfoDto userInfo;
}
