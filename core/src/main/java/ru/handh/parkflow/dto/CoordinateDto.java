package ru.handh.parkflow.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.parkflow.db.model.user.UserPositionEntity;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoordinateDto {

    @NotNull
    private Double lat;

    @NotNull
    private Double lon;

    @NotNull
    private Integer accuracy;

    public CoordinateDto(UserPositionEntity position) {
        if(position==null){
            return;
        }
        this.lat = position.getLat();
        this.lon = position.getLon();
        this.accuracy = position.getAccuracy();
    }
}
