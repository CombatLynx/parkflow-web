package ru.handh.parkflow.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import ru.handh.parkflow.db.model.user.UserEntity;

import java.util.List;
import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private CarDto car;

    private String userLanguage;
    private List<String> notApproved;
    private String country;
    private boolean active;
    private boolean havePermit;

    public UserInfoDto(UserEntity user, MessageSource messageSource) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.active = user.isActive();
        this.havePermit = user.isHavePermit();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();

        if (user.getLanguage() != null)
            this.userLanguage = user.getLanguage().getLanguage();
        if (user.getCountry() != null) {
            this.country = messageSource.getMessage(user.getCountry().getCountry().toLowerCase(), null, LocaleContextHolder.getLocale());
        }
        if (user.getCar() != null) {
            this.car.setModel(user.getCar().getModel());
            this.car.setMake(user.getCar().getMake());
            this.car.setNumber(user.getCar().getNumber());
            this.car.setColor(Optional.ofNullable(user.getCar().getColor())
                    .map(color ->
                            new CarColorDto(color, messageSource.getMessage(
                                    color.getType(),
                                    null,
                                    LocaleContextHolder.getLocale())))
                    .orElse(null));
        }

    }
}
