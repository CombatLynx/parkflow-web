package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GetRouteRequest {

    @NotNull
    private Double lat;

    @NotNull
    private Double lon;
}
