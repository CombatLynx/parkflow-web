package ru.handh.parkflow.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.dto.FeedbackDto;
import ru.handh.parkflow.dto.FeedbackReasonDto;

import java.util.List;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class GetFeedbackReasonResponse extends BaseResponse {

    private List<FeedbackReasonDto> feedbackReason;
}


