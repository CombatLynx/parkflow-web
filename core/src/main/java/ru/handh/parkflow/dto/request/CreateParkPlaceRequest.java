package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateParkPlaceRequest {

    @NotNull
    private Long createTime;

    @NotNull
    private Long leavingDelay;

    @NotNull
    private Double lat;

    @NotNull
    private Double lon;

    @NotNull
    private String type;
}
