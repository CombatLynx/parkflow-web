package ru.handh.parkflow.dto;

import lombok.Data;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import ru.handh.parkflow.db.model.auth.PhoneCodeEntity;

@Data
public class PhoneCodeDto {

    private Long id;
    private String country;
    private String isoCode;
    private String code;
    private String flag;

    public PhoneCodeDto(PhoneCodeEntity entity, MessageSource messageSource) {
        this.code = entity.getCode();
        this.id = entity.getId();
        this.flag = entity.getFlag();
        this.isoCode = entity.getCountry().getCountry();

        try {
            this.country = messageSource.getMessage(entity.getCountry().getCountry().toLowerCase(), null, LocaleContextHolder.getLocale());
        } catch(NoSuchMessageException e) {
            this.country = null;
        }
    }
}
