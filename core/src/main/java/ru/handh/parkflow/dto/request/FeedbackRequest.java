package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class FeedbackRequest {

    @NotEmpty(message = "{invalid_reason}")
    private String reason;

    @NotEmpty(message = "{invalid_message}")
    private String message;

}
