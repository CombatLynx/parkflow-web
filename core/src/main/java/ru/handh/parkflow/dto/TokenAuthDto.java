package ru.handh.parkflow.dto;

import lombok.Data;

@Data
public class TokenAuthDto {

    private Long id;
    private String token;
    private Long tokenExp;
    private Long cacheVersion;
    private boolean needUserInfo;
}
