package ru.handh.parkflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LeavingDelayDto {

    private Long id;
    private Long time;
}
