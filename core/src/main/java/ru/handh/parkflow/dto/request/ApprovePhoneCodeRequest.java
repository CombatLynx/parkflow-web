package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ApprovePhoneCodeRequest {

    @NotEmpty
    private String phone;
    @NotEmpty
    private String code;
}
