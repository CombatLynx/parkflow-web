package ru.handh.parkflow.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.dto.CarColorDto;

import java.util.List;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class GetCarColorsResponse extends BaseResponse {

    private List<CarColorDto> carColors;
}
