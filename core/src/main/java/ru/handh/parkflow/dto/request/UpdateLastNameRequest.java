package ru.handh.parkflow.dto.request;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class UpdateLastNameRequest {

    @Size(min = 1, max = 100, message = "{invalid_last_name}")
    private String lastName;
}
