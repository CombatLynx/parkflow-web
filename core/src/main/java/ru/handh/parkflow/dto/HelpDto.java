package ru.handh.parkflow.dto;

import lombok.Data;

@Data
public class HelpDto {

    private String title;
    private String subtitle;
    private String content;
}
