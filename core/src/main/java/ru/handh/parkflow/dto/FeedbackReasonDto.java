package ru.handh.parkflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.parkflow.db.model.feedback.FeedbackReasonType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackReasonDto {

    private FeedbackReasonType id;
    private String reason;

}
