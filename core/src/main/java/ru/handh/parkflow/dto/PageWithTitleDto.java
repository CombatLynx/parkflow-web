package ru.handh.parkflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class PageWithTitleDto<T> {

    private String title;
    private Page<T> content;
}
