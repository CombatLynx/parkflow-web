package ru.handh.parkflow.dto;

import lombok.Data;

@Data
public class FaqDto {

    private String title;
    private String subtitle;
    private String content;
}
