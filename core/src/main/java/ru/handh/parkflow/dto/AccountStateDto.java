package ru.handh.parkflow.dto;

import lombok.Data;

@Data
public class AccountStateDto {

    private Boolean confirmPhoneRequired;
    private Boolean fillUserInfoRequired;

    public AccountStateDto() {
        confirmPhoneRequired = false;
        fillUserInfoRequired = false;
    }
}
