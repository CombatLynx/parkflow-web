package ru.handh.parkflow.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.handh.parkflow.db.model.FeedbackEntity;


@Data
@NoArgsConstructor
public class FeedbackDto {

    private Long id;
    private String reason;
    private String message;


    public FeedbackDto(FeedbackEntity entity) {

        this.id = entity.getId();
        this.reason = entity.getReason();
        this.message = entity.getMessage();

    }
}
