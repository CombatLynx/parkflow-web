package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.FaqEntity;

public interface FaqRepository extends JpaRepository<FaqEntity, Long> {
}
