package ru.handh.parkflow.db.model.parking;

public enum ParkPlaceType {

    FREE,
    PAID,
    NEEDS_PERMIT
}
