package ru.handh.parkflow.db.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@MappedSuperclass
@Data
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID")
    protected Long id;

    @Column(name = "CREATED_DATE", nullable = false)
    protected LocalDateTime createDate;

    @Column(name = "MODIFIED_DATE", nullable = false)
    protected LocalDateTime modifiedDate;

    @PrePersist
    protected void prePersist() {
        this.setCreateDate(LocalDateTime.now());
        this.setModifiedDate(LocalDateTime.now());
    }

    @PreUpdate
    protected void preUpdate() {
        this.setModifiedDate(LocalDateTime.now());
    }
}
