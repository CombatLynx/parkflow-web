package ru.handh.parkflow.db.model.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;
import ru.handh.parkflow.db.model.auth.AuthInfoEntity;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "USER")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends BaseEntity {

    @Column(name = "PUSH_TOKEN")
    private String pushToken;

    @Column(name = "PLATFORM")
    private String platform;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "ACTIVE", columnDefinition = "BIT", length = 1, nullable = false)
    private boolean active = true;

    @Column(name = "HAVE_PERMIT", columnDefinition = "BIT", length = 1, nullable = false)
    private boolean havePermit = false;

    @ManyToOne
    @JoinColumn(name = "LANGUAGE_ID")
    private UserLanguageEntity language;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<AuthInfoEntity> authInfos;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
    private CarEntity car;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_POSITION_ID")
    private UserPositionEntity userPosition;

    @ManyToOne
    @JoinColumn(name = "COUNTRY_ID")
    private CountryEntity country;
}
