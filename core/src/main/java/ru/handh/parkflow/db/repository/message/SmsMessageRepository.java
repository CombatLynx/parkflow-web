package ru.handh.parkflow.db.repository.message;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.message.SmsMessageEntity;

public interface SmsMessageRepository extends JpaRepository<SmsMessageEntity, Long> {
}
