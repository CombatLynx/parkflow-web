package ru.handh.parkflow.db.model.exception;

public class RouteException extends Exception {

    public RouteException(String message) {
        super(message);
    }
}
