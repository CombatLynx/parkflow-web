package ru.handh.parkflow.db.model.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "SMS_QUEUE")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SmsQueueEntity extends MessageQueueEntity<SmsMessageEntity> {
}
