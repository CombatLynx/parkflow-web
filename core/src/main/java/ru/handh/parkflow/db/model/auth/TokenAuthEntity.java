package ru.handh.parkflow.db.model.auth;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;
import ru.handh.parkflow.db.model.user.UserEntity;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(
        name = "AUTH_TOKEN",
        uniqueConstraints = {@UniqueConstraint(columnNames = "USER_ID")}
)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class TokenAuthEntity extends BaseEntity {

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "EXPIRED")
    private OffsetDateTime expired;

    @OneToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity user;
}
