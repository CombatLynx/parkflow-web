package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.user.CarEntity;

import java.util.Optional;

public interface CarRepository extends JpaRepository<CarEntity, Long> {

    Optional<CarEntity> findByUserId(Long userId);
}
