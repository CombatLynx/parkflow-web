package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.user.CountryEntity;

public interface CountryRepository extends JpaRepository<CountryEntity, Long> {

    CountryEntity findByCountry(String country);
}
