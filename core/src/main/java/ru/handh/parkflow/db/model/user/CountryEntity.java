package ru.handh.parkflow.db.model.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "COUNTRY")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CountryEntity extends BaseEntity {

    @Column(name = "COUNTRY")
    private String country;
}
