package ru.handh.parkflow.db.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "LEGAL")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class LegalEntity extends BaseEntity {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "SUBTITLE")
    private String subtitle;

    @Column(name = "CONTENT")
    private String content;
}
