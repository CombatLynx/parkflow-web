package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.property.PropertyEntity;
import ru.handh.parkflow.db.model.property.PropertyName;

public interface PropertyRepository extends JpaRepository<PropertyEntity, Long> {

    PropertyEntity findByName(PropertyName name);
}
