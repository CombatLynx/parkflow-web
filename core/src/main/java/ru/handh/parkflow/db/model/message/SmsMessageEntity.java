package ru.handh.parkflow.db.model.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "SMS_MESSAGE")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SmsMessageEntity extends BaseMessageEntity {

    @Column(name = "TEXT")
    @Type(type = "text")
    private String text;

    public SmsMessageEntity(String to, String text) {
        this.to = to;
        this.text = text;
    }
}
