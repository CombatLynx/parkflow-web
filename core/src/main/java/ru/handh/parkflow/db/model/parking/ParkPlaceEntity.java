package ru.handh.parkflow.db.model.parking;

import lombok.*;
import ru.handh.parkflow.db.model.BaseEntity;
import ru.handh.parkflow.db.model.user.UserEntity;

import javax.persistence.*;
import java.time.OffsetDateTime;

import static javax.persistence.EnumType.STRING;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "PARK_PLACE")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ParkPlaceEntity extends BaseEntity {

    @Enumerated(STRING)
    @Column(name = "STATUS")
    ParkPlaceStatus status;

    @Column(name = "LONGITUDE")
    Double lon;

    @Column(name = "LATITUDE")
    Double lat;

    @Column(name = "CREATE_TIME")
    OffsetDateTime createTime;

    @Column(name = "LEAVING_DELAY")
    Long leavingDelay;

    @Enumerated(STRING)
    @Column(name = "TYPE")
    ParkPlaceType type;

    @ManyToOne
    @JoinColumn(name = "OFFER_USER_ID")
    UserEntity offerUser;

    @ManyToOne
    @JoinColumn(name = "CLAIM_USER_ID")
    UserEntity claimUser;
}
