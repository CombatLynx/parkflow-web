package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.auth.TokenAuthEntity;
import ru.handh.parkflow.db.model.user.UserEntity;

import java.util.Optional;

public interface TokenAuthRepository extends JpaRepository<TokenAuthEntity, Long> {

    Optional<TokenAuthEntity> findByToken(String token);

    Optional<TokenAuthEntity> findByUser(UserEntity user);
}
