package ru.handh.parkflow.db.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "FEEDBACK")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FeedbackEntity extends BaseEntity  {

    @Column(name = "REASON")
    private String reason;

    @Column(name = "MESSAGE")
    private String message;
}
