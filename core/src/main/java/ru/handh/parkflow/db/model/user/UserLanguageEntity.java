package ru.handh.parkflow.db.model.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "USER_LANGUAGE")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserLanguageEntity extends BaseEntity {

    @Column(name = "LANGUAGE")
    private String language;
}
