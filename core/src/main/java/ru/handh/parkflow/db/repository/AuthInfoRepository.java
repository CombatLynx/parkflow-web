package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.auth.AuthInfoEntity;

import java.util.List;

public interface AuthInfoRepository extends JpaRepository<AuthInfoEntity, Long> {

    List<AuthInfoEntity> findByUserId(Long userId);
}
