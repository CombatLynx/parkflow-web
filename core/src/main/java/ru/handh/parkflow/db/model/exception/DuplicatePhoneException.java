package ru.handh.parkflow.db.model.exception;

public class DuplicatePhoneException extends Exception {

    public DuplicatePhoneException(String message) {
        super(message);
    }
}
