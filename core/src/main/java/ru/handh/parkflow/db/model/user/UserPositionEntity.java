package ru.handh.parkflow.db.model.user;

import lombok.*;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.*;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "USER_POSITION")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserPositionEntity extends BaseEntity {

    @Column(name = "LAT")
    private Double lat;

    @Column(name = "LON")
    private Double lon;

    @Column(name = "ACCURACY")
    private Integer accuracy;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity user;
}
