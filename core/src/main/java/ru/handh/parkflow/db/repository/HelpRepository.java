package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.HelpEntity;

public interface HelpRepository extends JpaRepository<HelpEntity, Long> {
}
