package ru.handh.parkflow.db.model.parking;

public enum ParkPlaceStatus {

    LOOKING,
    ACTIVE,
    CANCELED,
    FINISHED,
    EXPIRED
}
