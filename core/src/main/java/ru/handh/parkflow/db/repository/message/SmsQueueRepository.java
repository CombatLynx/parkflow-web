package ru.handh.parkflow.db.repository.message;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.message.SmsQueueEntity;

public interface SmsQueueRepository extends JpaRepository<SmsQueueEntity, Long> {
}
