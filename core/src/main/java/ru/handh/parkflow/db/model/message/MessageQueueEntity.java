package ru.handh.parkflow.db.model.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class MessageQueueEntity<T extends BaseMessageEntity> extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "MESSAGE_ID")
    protected T message;

    @Column(name = "ATTEMPTS")
    protected Integer attempts;

    @Column(name = "LAST_ERROR")
    protected String lastError;

    @Column(name = "LAST_ATTEMPT")
    protected OffsetDateTime lastAttempt;
}
