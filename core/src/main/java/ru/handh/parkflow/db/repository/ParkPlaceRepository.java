package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.handh.parkflow.db.model.parking.ParkPlaceEntity;

import java.util.List;

public interface ParkPlaceRepository extends JpaRepository<ParkPlaceEntity, Long> {

    @Query("select pp from ParkPlaceEntity pp where pp.status = 'LOOKING' and ((pp.lat - :lat)*(pp.lat - :lat) + (pp.lon - :lon)*(pp.lon - :lon)) < :radius*:radius")
    List<ParkPlaceEntity> getInRadius(@Param("lat") Double lat, @Param("lon") Double lon, @Param("radius") Double radius);
}
