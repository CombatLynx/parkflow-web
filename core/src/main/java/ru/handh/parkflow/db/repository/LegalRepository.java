package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.LegalEntity;

public interface LegalRepository extends JpaRepository<LegalEntity, Long> {
}
