package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.auth.PhoneCodeEntity;

public interface PhoneCodeRepository extends JpaRepository<PhoneCodeEntity, Long> {
}
