package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.user.UserEntity;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByPhone(String phone);
}
