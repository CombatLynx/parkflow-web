package ru.handh.parkflow.db.model.exception;

public class SchedulerTasksException extends Exception {

    public SchedulerTasksException(String message) {
        super(message);
    }
}
