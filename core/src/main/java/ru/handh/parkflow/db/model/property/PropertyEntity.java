package ru.handh.parkflow.db.model.property;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.*;

@Data
@Entity
@Table(name = "PROPERTY")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PropertyEntity extends BaseEntity {

    @Column(name = "NAME")
    @Enumerated(EnumType.STRING)
    private PropertyName name;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private PropertyType type;

    @Column(name = "EDITABLE")
    private Boolean editable;

    @Column(name = "DESCRIPTION")
    private String description;
}
