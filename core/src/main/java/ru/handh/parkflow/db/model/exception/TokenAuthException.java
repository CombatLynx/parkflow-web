package ru.handh.parkflow.db.model.exception;

import org.springframework.security.core.AuthenticationException;

public class TokenAuthException  extends AuthenticationException {

    public TokenAuthException(String message) {
        super(message);
    }
}
