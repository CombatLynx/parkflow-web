package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.user.UserLanguageEntity;

public interface UserLanguageRepository extends JpaRepository<UserLanguageEntity, Long> {

    UserLanguageEntity findByLanguage(String lang);
}
