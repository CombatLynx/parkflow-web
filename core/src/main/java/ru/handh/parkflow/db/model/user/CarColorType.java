package ru.handh.parkflow.db.model.user;

import java.util.Arrays;

public enum CarColorType {

    CAR_COLOR_WHITE("car_color.white"),
    CAR_COLOR_BLACK("car_color.black"),
    CAR_COLOR_SILVER("car_color.silver"),
    CAR_COLOR_GREY("car_color.grey"),
    CAR_COLOR_BLUE("car_color.blue"),
    CAR_COLOR_RED("car_color.red"),
    CAR_COLOR_GREEN("car_color.green"),
    CAR_COLOR_BEIGE("car_color.beige"),
    CAR_COLOR_YELLOW("car_color.yellow"),
    CAR_COLOR_OTHER("car_color.other");

    private String type;

    CarColorType(String type) {
        this.type = type;
    }

    public static CarColorType getType(final String inputType) {
        return Arrays.stream(values()).filter(e -> inputType.equals(e.type)).findFirst().orElse(null);
    }

    public String getType() {
        return type;
    }
}
