package ru.handh.parkflow.db.model.auth;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "APPROVE_PHONE_CODE")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ApprovePhoneCodeEntity extends BaseEntity {

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "CODE")
    private String code;

    @Column(name = "LAST_REQUEST")
    private OffsetDateTime lastRequest;
}
