package ru.handh.parkflow.db.model.exception;

public class UserException extends Exception {

    public UserException(String message) {
        super(message);
    }
}
