package ru.handh.parkflow.db.model.exception;

public class ApproveCodeException extends Exception {

    public ApproveCodeException(String message) {
        super(message);
    }
}
