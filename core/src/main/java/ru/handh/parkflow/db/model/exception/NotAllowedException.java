package ru.handh.parkflow.db.model.exception;

public class NotAllowedException extends Exception {

    public NotAllowedException(String message) {
        super(message);
    }
}
