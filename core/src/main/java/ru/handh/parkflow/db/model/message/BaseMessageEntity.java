package ru.handh.parkflow.db.model.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class BaseMessageEntity extends BaseEntity {

    @Column(name = "`TO`")
    protected String to;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    protected MessageStatus status;

    @Column(name = "ERROR")
    protected String error;

    @Column(name = "SEND_TIME")
    protected OffsetDateTime sendTime;
}
