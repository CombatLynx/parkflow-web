package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.user.UserPositionEntity;

public interface PositionRepository extends JpaRepository<UserPositionEntity, Long> {
}
