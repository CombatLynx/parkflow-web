package ru.handh.parkflow.db.model.feedback;

import java.util.Arrays;

public enum FeedbackReasonType {

    FEEDBACK_REASON_I_DID_NOT_GET_MY_REWARD("feedback_reason.i_did_not_get_my_reward"),
    FEEDBACK_REASON_SPACE_WAS_NOT_AVAILABLE("feedback_reason.space_was_not_available");

    private String type;

    FeedbackReasonType(String type) {
        this.type = type;
    }

    public static FeedbackReasonType getFeedbackReason(final String inputType) {
        return Arrays.stream(values()).filter(e -> inputType.equals(e.type)).findFirst().orElse(null);
    }

    public String getType() {
        return type;
    }
}
