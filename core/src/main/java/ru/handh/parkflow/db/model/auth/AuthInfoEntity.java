package ru.handh.parkflow.db.model.auth;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;
import ru.handh.parkflow.db.model.user.UserEntity;

import javax.persistence.*;

@Data
@Entity
@Table(name = "AUTH_INFO")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AuthInfoEntity extends BaseEntity {

    @Column(name = "AUTH_INFO")
    private String authInfo;

    @Column(name = "APPROVED", columnDefinition = "BIT DEFAULT 1")
    private Boolean approved;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity user;
}
