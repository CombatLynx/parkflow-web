package ru.handh.parkflow.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.parkflow.db.model.auth.ApprovePhoneCodeEntity;

import java.util.Optional;

public interface ApprovePhoneCodeRepository extends JpaRepository<ApprovePhoneCodeEntity, Long> {

    Optional<ApprovePhoneCodeEntity> findByPhone(String phone);
}
