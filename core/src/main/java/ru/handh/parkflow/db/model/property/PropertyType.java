package ru.handh.parkflow.db.model.property;

public enum PropertyType {
    SYSTEM,
    LANDING,
    VERSION
}
