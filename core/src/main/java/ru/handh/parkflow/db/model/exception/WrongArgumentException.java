package ru.handh.parkflow.db.model.exception;

public class WrongArgumentException extends Exception {

    public WrongArgumentException(String message) {
        super(message);
    }
}
