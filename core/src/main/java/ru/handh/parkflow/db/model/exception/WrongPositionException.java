package ru.handh.parkflow.db.model.exception;

public class WrongPositionException extends Exception {

    public WrongPositionException(String message) {
        super(message);
    }
}
