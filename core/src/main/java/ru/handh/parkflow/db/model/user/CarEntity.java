package ru.handh.parkflow.db.model.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "CAR")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CarEntity extends BaseEntity {

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "MAKE")
    private String make;

    @Column(name = "MODEL")
    private String model;

    @Enumerated(STRING)
    @Column(name = "COLOR")
    private CarColorType color;

    @OneToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity user;
}
