package ru.handh.parkflow.db.model.exception;

import javassist.NotFoundException;

public class EntityNotFoundException extends NotFoundException {

    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
