package ru.handh.parkflow.db.model.auth;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.handh.parkflow.db.model.BaseEntity;
import ru.handh.parkflow.db.model.user.CountryEntity;

import javax.persistence.*;

@Data
@Entity
@Table(name = "PHONE_CODE")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PhoneCodeEntity extends BaseEntity {

    @Column(name = "CODE")
    private String code;

    @Column(name = "FLAG")
    private String flag;

    @OneToOne
    @JoinColumn(name = "COUNTRY_ID")
    private CountryEntity country;
}
