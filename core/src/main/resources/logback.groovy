import net.logstash.logback.encoder.LogstashEncoder
import net.logstash.logback.fieldnames.LogstashFieldNames
import net.logstash.logback.stacktrace.ShortenedThrowableConverter

def LOG_PATH = "log"

appender("Console-Appender", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%msg%n"
    }
}

appender("jsonAppender", RollingFileAppender) {
    file = "${LOG_PATH}/log.json"
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${LOG_PATH}/archive/log.%d{yyyy-MM-dd}.zip"
        maxHistory = 30
        totalSizeCap = "1GB"
    }
    encoder(LogstashEncoder) {
        fieldNames(LogstashFieldNames) {
            timestamp = '@time'
            version = '[ignore]'
            message = 'msg'
            logger = 'logger'
            thread = 'thread'
            levelValue = '[ignore]'
        }
         throwableConverter(ShortenedThrowableConverter) {
             maxDepthPerThrowable = 20
             maxLength = 8192
             shortenedClassNameLength = 35
             exclude = /sun\..*/
             exclude = /java\..*/
             exclude = /groovy\..*/
             exclude = /com\.sun\..*/
             rootCauseFirst = true
        }
    }
}

root(INFO, ["Console-Appender", "jsonAppender"])