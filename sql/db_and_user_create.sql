CREATE DATABASE parkflow CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE DATABASE parkflow_quartz CHARACTER SET utf8 COLLATE utf8_general_ci;
create user 'parkflow'@'%' identified by 'parkflow';
grant all privileges on parkflow.* to 'parkflow'@'%';
grant all privileges on parkflow_quartz.* to 'parkflow'@'%';